#include "FS.h"
#include "LITTLEFS.h"

#define SPIFFS LITTLEFS
#define FORMAT_LITTLEFS_IF_FAILED true

String fileName[20];
String directoryName[20];
int countFileName = 0;
int countDirecName = 0;


void listDir(fs::FS &fs, const char * dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.print (file.name());
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.print(file.size());
    }
    file = root.openNextFile();
  }
}

void createDir(fs::FS &fs, const char * path) {
  Serial.printf("Creating Dir: %s\n", path);
  if (fs.mkdir(path)) {
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

void removeDir(fs::FS &fs, const char * path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void readFile(fs::FS &fs, const char * path) {
  Serial.printf("Reading file: %s\n", path);

  File file = fs.open(path);
  if (!file) {
    Serial.println("Failed to open file for reading");
    return;
  }

  Serial.print("Read from file: ");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if (!file) {
    Serial.println("Failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if (!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if (file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}

void renameFile(fs::FS &fs, const char * path1, const char * path2) {
  Serial.printf("Renaming file %s to %s\n", path1, path2);
  if (fs.rename(path1, path2)) {
    Serial.println("File renamed");
  } else {
    Serial.println("Rename failed");
  }
}

void deleteFile(fs::FS &fs, const char * path) {
  Serial.printf("Deleting file: %s\n", path);
  if (fs.remove(path)) {
    Serial.println("File deleted");
  } else {
    Serial.println("Delete failed");
  }
}

void getDir(fs::FS &fs, const char * dirname, uint8_t levels) {

  File root = fs.open(dirname);
  File file = root.openNextFile();

  while (file) {
    if (file.isDirectory()) {
      directoryName[countDirecName] = String(file.name());
      countDirecName++;
      if (levels) {
        getDir(fs, file.name(), levels - 1);
      }
    }
    else {
      fileName[countFileName] = String(file.name());
      ++countFileName;
    }
    file = root.openNextFile();
  }
}

void deleteDir() {
  for (int j = 0; j < countFileName; j++) {
    int str_len = fileName[j].length() + 1;
    char char_array[str_len];
    fileName[j].toCharArray(char_array, str_len);
    deleteFile(SPIFFS, char_array);
  }

  for (int i = 0; i < countDirecName; i++) {
    int str_len = directoryName[i].length() + 1;
    char char_array[str_len];
    directoryName[i].toCharArray(char_array, str_len);
    removeDir(SPIFFS, char_array);
  }
}
void setup() {
  Serial.begin(115200);

  if (!SPIFFS.begin(1)) {
    Serial.println("LITTLEFS Mount Failed");
    return;
  }

  Serial.println("----create a new dir----");
  createDir(SPIFFS, "/mydir");

  Serial.println("----create and work with file----");
  writeFile(SPIFFS, "/mydir/hello.txt", "Hello ");
  appendFile(SPIFFS, "/mydir/hello.txt", "World!\n");
  writeFile(SPIFFS, "/hell1o.txt", "Hello ");
  appendFile(SPIFFS, "/hell1o.txt", "World!\n");
  writeFile(SPIFFS, "/hell2o.txt", "Hello ");
  appendFile(SPIFFS, "/hell2o.txt", "World!\n");
  Serial.println("----list 2----");
  listDir(SPIFFS, "/", 1);
  Serial.println("");
  Serial.println("----remove dir after deleting file----");
  getDir(SPIFFS, "/", 1);
  deleteDir();
  Serial.println("----list 3----");
  listDir(SPIFFS, "/", 1);

  Serial.println( "Test complete" );

}

void loop() {

}
