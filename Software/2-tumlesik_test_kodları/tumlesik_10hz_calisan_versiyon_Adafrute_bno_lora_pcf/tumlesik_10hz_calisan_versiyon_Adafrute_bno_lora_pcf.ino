#include "FS.h"
#include "LITTLEFS.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_H3LIS331.h>
#include "ms5611_driver.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO08x.h>
#include <Wire.h>
#include <RadioLib.h>
#include "SPI.h"
#include "SparkFun_Ublox_Arduino_Library.h"
#include <MicroNMEA.h>
#include "ESP32TimerInterrupt.h"
//#include "PCF8574.h"
#include "PCA9539.h"
//PCF8574 PCF_01(0x20);
PCA9539 ioport(0x76);
// #define TIMER_INTERRUPT_DEBUG 1
// #define _TIMERINTERRUPT_LOGLEVEL_ 4

char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));
SFE_UBLOX_GPS myGPS;
int RXD2 = 25;
int TXD2 = 26;

// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33);

float H3LIS331_data[3];
float Ms5611_data[2];
float bnoAccel[3];
float bnoGyro[3];
float bnoMag[3];
float bnoRaw[3];
long GPS_data[3];
int GPS_time[6];
uint32_t SIV;

//ESP32Timer ITimer1(1);
int timer100 = 0;
int timer100_2=0;
// void IRAM_ATTR TimerHandler1(void) {
//   timer100++;
//   timer100_2++;
// }

//int RXD2 = 25;
//int TXD2 = 26;



void longAddByte(long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}
struct Data {
  byte byteArray[200];
  int counter = 0;
};
Data data;


#define BNO08X_CS 5
#define BNO08X_INT 16
#define BNO08X_RESET 2

Adafruit_BNO08x bno08x(BNO08X_RESET);
sh2_SensorValue_t sensorValue;

struct euler_t {
  float yaw;
  float pitch;
  float roll;
} ypr;

uint8_t MS_CS = 17;
uint8_t accel200gCSPin = 27;
baro_ms5611 MS5611(MS_CS);
Adafruit_H3LIS331 lis = Adafruit_H3LIS331();

File file;
String All_data1[10];
String All_data2[10];
String All_data3[10];
String All_data4[10];
String All_data_flash1[10];
String All_data_flash2[10];
String All_data_flash3[10];
String All_data_flash4[10];
String header = "400gAcceloX,400gAcceloY,400gAcceloZ,MS5611Temp,MS5611Pressure,BNOAccelX,BNOAccelY,BNOAccelZ,BNOGyroX,BNOGyroY,BNOGyroZ,BNOMagX,BNOMagY,BNOMagZ,Yaw,Pitch,Roll,GpsLatitude,GpsLongitude,GpsAltitude,GpsDate,GpsTime\n";
int hafiza = 0;
int i = 0;

#define SPIFFS LITTLEFS
#define FORMAT_LITTLEFS_IF_FAILED true

bool flashCheck1 = 0;
bool flashCheck2 = 0;
bool flashCheck3 = 0;
bool flashCheck4 = 0;
bool writeCheck1 = 0;
bool writeCheck2 = 0;
bool writeCheck3 = 0;
bool writeCheck4 = 0;
uint8_t dataCheck = 0;
bool writeStart = 0;

TaskHandle_t Task1;

void setup() {
  Serial.begin(115200);
  pinMode(BNO08X_CS, 1);
  pinMode(accel200gCSPin, 1);
  pinMode(MS_CS, 1);
  digitalWrite(BNO08X_CS, 1);
  digitalWrite(accel200gCSPin, 1);
  digitalWrite(MS_CS, 1);
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin(868.0, 500.0, 7, 5, 0x34, 20);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }

  SPIFFS.begin(1);
  //flashRead();
  flashClean();
  //baslık dosyasını yaz
  writeFile(SPIFFS, "/Data.txt", header);
  flashCreatPart();

  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  myGPS.begin(Serial2);
  myGPS.setSerialRate(38400);
  Serial.println("Trying 38400 baud");
  Serial2.begin(38400, SERIAL_8N1, RXD2, TXD2);
  myGPS.begin(Serial2);
  //This will pipe all NMEA sentences to the serial port so we can see them
  myGPS.setNMEAOutputPort(Serial);
  myGPS.setNavigationFrequency(10);

  Serial.println("Adafruit BNO08x test!");
  if (!bno08x.begin_SPI(BNO08X_CS, BNO08X_INT)) {
    Serial.println("Failed to find BNO08x chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("BNO08x Found!");

  setReports();
  Serial.println("Reading events");
  delay(100);

  if (!lis.begin_SPI(accel200gCSPin)) {
    Serial.println("Couldnt start");
    while (1) yield();
  }
  lis.setRange(H3LIS331_RANGE_100_G);  // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_50_HZ);

  //bme.begin();
  MS5611.Initialize();
  Wire.begin(21,22);
  
  ioport.pinMode(bb3, OUTPUT);
  ioport.pinMode(bb4, OUTPUT);
  ioport.digitalWrite(bb3, LOW);
  ioport.digitalWrite(bb4, LOW);
  delay(100);
  //ITimer1.attachInterruptInterval(20000, TimerHandler1);
  xTaskCreatePinnedToCore(
    Task1code, /* Task function. */
    "Task1",   /* name of task. */
    10000,     /* Stack size of task */
    NULL,      /* parameter of the task */
    1,         /* priority of the task */
    &Task1,    /* Task handle to keep track of created task */
    0);        /* pin task to core 0 */
}

void Task1code(void *pvParameters) {
  //Serial.print("Task1 running on core ");
  //Serial.println(xPortGetCoreID());

  for (;;) {
    vTaskDelay(1);
    flashWrite();
  }
}
unsigned long start = 0;
unsigned long start1 = 0;
unsigned long end = 0;

// Here is where you define the sensor outputs you want to receive
void setReports(void) {
  Serial.println("Setting desired reports");
  if (!bno08x.enableReport(SH2_ACCELEROMETER)) {
    Serial.println("Could not enable accelerometer");
  }
  if (!bno08x.enableReport(SH2_GYROSCOPE_CALIBRATED)) {
    Serial.println("Could not enable gyroscope");
  }
  if (!bno08x.enableReport(SH2_MAGNETIC_FIELD_CALIBRATED)) {
    Serial.println("Could not enable magnetic field calibrated");
  }
  if (!bno08x.enableReport(SH2_ROTATION_VECTOR)) {
    Serial.println("Could not enable rotation vector");
  }
}
void quaternionToEuler(float qr, float qi, float qj, float qk, euler_t* ypr, bool degrees = false) {

  float sqr = sq(qr);
  float sqi = sq(qi);
  float sqj = sq(qj);
  float sqk = sq(qk);

  ypr->yaw = atan2(2.0 * (qi * qj + qk * qr), (sqi - sqj - sqk + sqr));
  ypr->pitch = asin(-2.0 * (qi * qk - qj * qr) / (sqi + sqj + sqk + sqr));
  ypr->roll = atan2(2.0 * (qj * qk + qi * qr), (-sqi - sqj + sqk + sqr));

  if (degrees) {
    ypr->yaw *= RAD_TO_DEG;
    ypr->pitch *= RAD_TO_DEG;
    ypr->roll *= RAD_TO_DEG;
  }
}

void SFE_UBLOX_GPS::processNMEA(char incoming) {
  //Take the incoming char from the Ublox I2C port and pass it on to the MicroNMEA lib
  //for sentence cracking
  nmea.process(incoming);
}
int a=0;
void loop() {
  delay(1);
  if (!bno08x.getSensorEvent(&sensorValue)) {
    return;
  }

  switch (sensorValue.sensorId) {
    start = millis();
    case SH2_ACCELEROMETER:
      bnoAccel[0] =(sensorValue.un.accelerometer.x);
      bnoAccel[1] =(sensorValue.un.accelerometer.y);
      bnoAccel[2] =(sensorValue.un.accelerometer.z);
      break;
    case SH2_GYROSCOPE_CALIBRATED:
      bnoGyro[0] = (sensorValue.un.gyroscope.x);
      bnoGyro[1] = (sensorValue.un.gyroscope.y);
      bnoGyro[2] = (sensorValue.un.gyroscope.z);
      break;
    case SH2_MAGNETIC_FIELD_CALIBRATED:
      bnoMag[0] = (sensorValue.un.magneticField.x);
      bnoMag[1] = (sensorValue.un.magneticField.y);
      bnoMag[2] = (sensorValue.un.magneticField.z);
      break;
    case SH2_ROTATION_VECTOR:
      quaternionToEuler(sensorValue.un.rotationVector.real, sensorValue.un.rotationVector.i, sensorValue.un.rotationVector.j, sensorValue.un.rotationVector.k, &ypr, true);
      bnoRaw[0] = (ypr.yaw);
      bnoRaw[1] = (ypr.pitch);
      bnoRaw[2] = (ypr.roll);
      break;
      end = millis();
    if (end - start > 3) Serial.printf("okuma suresi bno0 : %d \n", end - start);
  }

  if (millis() - start1 >= 100) {

    start1 = millis();
    start = millis();
    sensors_event_t event;
    lis.getEvent(&event);
    H3LIS331_data[0] = event.acceleration.x / SENSORS_GRAVITY_STANDARD;
    H3LIS331_data[1] = event.acceleration.y / SENSORS_GRAVITY_STANDARD;
    H3LIS331_data[2] = event.acceleration.z / SENSORS_GRAVITY_STANDARD;
    digitalWrite(accel200gCSPin, 1);
    end = millis();
    if (end - start > 3) Serial.printf("okuma suresi 400g : %d \n", end - start);

    start = millis();
    //Ms5611_data[0] = bme.readTemperature();
    //Ms5611_data[1] = bme.readPressure() / 100.0F;
    MS5611.updateTemperature();
    MS5611.getPressure();
    Ms5611_data[0] = MS5611.getTemperature_degC();
    Ms5611_data[1] = MS5611.getPressure_mbar();
    end = millis();
    if (end - start > 3) Serial.printf("okuma suresi bmp  : %d \n", end - start);

    start = millis();
    int state = radio.transmit("Hello World!");

    

    if (state == ERR_NONE) {
      // the packet was successfully transmitted
      //Serial.println(F("success!"));

      // print measured data rate
      // Serial.print(F("[SX1262] Datarate:\t"));
      // Serial.print(radio.getDataRate());
      // Serial.println(F(" bps"));

    } else if (state == ERR_PACKET_TOO_LONG) {
      // the supplied packet was longer than 256 bytes
      Serial.println(F("too long!"));

    } else if (state == ERR_TX_TIMEOUT) {
      // timeout occured while transmitting packet
      Serial.println(F("timeout!"));

    } else {
      // some other error occurred
      Serial.print(F("failed, code "));
      Serial.println(state);
    }

    end = millis();
    if (end - start > 20) Serial.printf("okuma suresi Lora  : %d \n", end - start);

    start = millis();
    myGPS.checkUblox();  //See if new data is available. Process bytes as they come in.
    if (nmea.isValid() == true) {
      GPS_data[0] = nmea.getLatitude();
      GPS_data[1] = nmea.getLongitude();
      nmea.getAltitude(GPS_data[2]);
      SIV = nmea.getNumSatellites();
      GPS_time[0] = nmea.getYear();
      GPS_time[1] = nmea.getMonth();
      GPS_time[2] = nmea.getDay();
      GPS_time[3] = nmea.getHour();
      GPS_time[4] = nmea.getMinute();
      GPS_time[5] = nmea.getSecond();
    }
    end = millis();
    if (end - start > 10) Serial.printf("okuma suresi GPS  : %d \n", end - start);
    start = millis();
    writeData();
    end = millis();
    if (end - start > 10) Serial.printf("okuma suresi yazma: %d \n", end - start);
    if(a==0){
    ioport.digitalWrite(bb3, HIGH);  
    ioport.digitalWrite(bb4, LOW);
    a++;    
    }
    else{
    ioport.digitalWrite(bb3, LOW);
    ioport.digitalWrite(bb4, HIGH);
    a=0;
    }
    if (end - start1 > 30) Serial.printf("TOPLAM  yazma: %d \n", end - start1);
  }
}
void writeData() {

  if (dataCheck == 0) {
    while (flashCheck1) {
      vTaskDelay(1);
    }
    All_data1[i] = String(H3LIS331_data[0]) + "," + String(H3LIS331_data[1]) + "," + String(H3LIS331_data[2]) + ",";
    All_data1[i] += String(Ms5611_data[0]) + "," + String(Ms5611_data[1]) + ",";
    All_data1[i] += String(bnoAccel[0]) + "," + String(bnoAccel[1]) + "," + String(bnoAccel[2]) + ",";
    All_data1[i] += String(bnoGyro[0]) + "," + String(bnoGyro[1]) + "," + String(bnoGyro[2]) + ",";
    All_data1[i] += String(bnoMag[0]) + "," + String(bnoMag[1]) + "," + String(bnoMag[2]) + ",";
    All_data1[i] += String(bnoRaw[0]) + "," + String(bnoRaw[1]) + "," + String(bnoRaw[2]) + ",";
    All_data1[i] += String(GPS_data[0]) + "," + String(GPS_data[1]) + "," + String(GPS_data[2]) + ",";
    All_data1[i] += String(SIV) + ",";
    All_data1[i] += String(GPS_time[0]) + "-" + String(GPS_time[1]) + "-" + String(GPS_time[2]) + ",";
    All_data1[i] += String(GPS_time[3]) + "." + String(GPS_time[4]) + "." + String(GPS_time[5]) + "\n";
    Serial.println(All_data1[i]);
    if (i == 9) {
      i = 0;
      flashCheck1 = 1;
      dataCheck = 1;
    } else {
      i++;
    }
  } else if (dataCheck == 1) {
    while (flashCheck2) {
      vTaskDelay(1);
    }
    All_data2[i] = String(H3LIS331_data[0]) + "," + String(H3LIS331_data[1]) + "," + String(H3LIS331_data[2]) + ",";
    All_data2[i] += String(Ms5611_data[0]) + "," + String(Ms5611_data[1]) + ",";
    All_data2[i] += String(bnoAccel[0]) + "," + String(bnoAccel[1]) + "," + String(bnoAccel[2]) + ",";
    All_data2[i] += String(bnoGyro[0]) + "," + String(bnoGyro[1]) + "," + String(bnoGyro[2]) + ",";
    All_data2[i] += String(bnoMag[0]) + "," + String(bnoMag[1]) + "," + String(bnoMag[2]) + ",";
    All_data2[i] += String(bnoRaw[0]) + "," + String(bnoRaw[1]) + "," + String(bnoRaw[2]) + ",";
    All_data2[i] += String(GPS_data[0]) + "," + String(GPS_data[1]) + "," + String(GPS_data[2]) + ",";
    All_data2[i] += String(SIV) + ",";
    All_data2[i] += String(GPS_time[0]) + "-" + String(GPS_time[1]) + "-" + String(GPS_time[2]) + ",";
    All_data2[i] += String(GPS_time[3]) + "." + String(GPS_time[4]) + "." + String(GPS_time[5]) + "\n";

    if (i == 9) {
      i = 0;
      flashCheck2 = 1;
      dataCheck = 2;
    } else {
      i++;
    }
  } else if (dataCheck == 2) {
    while (flashCheck3) {
      vTaskDelay(1);
    }
    All_data3[i] = String(H3LIS331_data[0]) + "," + String(H3LIS331_data[1]) + "," + String(H3LIS331_data[2]) + ",";
    All_data3[i] += String(Ms5611_data[0]) + "," + String(Ms5611_data[1]) + ",";
    All_data3[i] += String(bnoAccel[0]) + "," + String(bnoAccel[1]) + "," + String(bnoAccel[2]) + ",";
    All_data3[i] += String(bnoGyro[0]) + "," + String(bnoGyro[1]) + "," + String(bnoGyro[2]) + ",";
    All_data3[i] += String(bnoMag[0]) + "," + String(bnoMag[1]) + "," + String(bnoMag[2]) + ",";
    All_data3[i] += String(bnoRaw[0]) + "," + String(bnoRaw[1]) + "," + String(bnoRaw[2]) + ",";
    All_data3[i] += String(GPS_data[0]) + "," + String(GPS_data[1]) + "," + String(GPS_data[2]) + ",";
    All_data3[i] += String(SIV) + ",";
    All_data3[i] += String(GPS_time[0]) + "-" + String(GPS_time[1]) + "-" + String(GPS_time[2]) + ",";
    All_data3[i] += String(GPS_time[3]) + "." + String(GPS_time[4]) + "." + String(GPS_time[5]) + "\n";

    if (i == 9) {
      i = 0;
      flashCheck3 = 1;
      dataCheck = 3;
    } else {
      i++;
    }
  } else if (dataCheck == 3) {
    while (flashCheck4) {
      vTaskDelay(1);
    }
    All_data4[i] = String(H3LIS331_data[0]) + "," + String(H3LIS331_data[1]) + "," + String(H3LIS331_data[2]) + ",";
    All_data4[i] += String(Ms5611_data[0]) + "," + String(Ms5611_data[1]) + ",";
    All_data4[i] += String(bnoAccel[0]) + "," + String(bnoAccel[1]) + "," + String(bnoAccel[2]) + ",";
    All_data4[i] += String(bnoGyro[0]) + "," + String(bnoGyro[1]) + "," + String(bnoGyro[2]) + ",";
    All_data4[i] += String(bnoMag[0]) + "," + String(bnoMag[1]) + "," + String(bnoMag[2]) + ",";
    All_data4[i] += String(bnoRaw[0]) + "," + String(bnoRaw[1]) + "," + String(bnoRaw[2]) + ",";
    All_data4[i] += String(GPS_data[0]) + "," + String(GPS_data[1]) + "," + String(GPS_data[2]) + ",";
    All_data4[i] += String(SIV) + ",";
    All_data4[i] += String(GPS_time[0]) + "-" + String(GPS_time[1]) + "-" + String(GPS_time[2]) + ",";
    All_data4[i] += String(GPS_time[3]) + "." + String(GPS_time[4]) + "." + String(GPS_time[5]) + "\n";

    if (i == 9) {
      i = 0;
      flashCheck4 = 1;
      dataCheck = 0;
    } else {
      i++;
    }
  }
}

void flashWrite() {

  if (flashCheck1 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash1[j] = All_data1[j];
    }
    flashCheck1 = 0;
    writeCheck1 = 1;
  }

  if (flashCheck2 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash2[j] = All_data2[j];
    }
    flashCheck2 = 0;
    writeCheck2 = 1;
  }

  if (flashCheck3 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash3[j] = All_data3[j];
    }
    flashCheck3 = 0;
    writeCheck3 = 1;
  }

  if (flashCheck4 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash4[j] = All_data4[j];
    }
    flashCheck4 = 0;
    writeCheck4 = 1;
  }

  if (writeCheck1 == 1) {
    flashWriteData(All_data_flash1);
    writeCheck1 = 0;
  }

  if (writeCheck2 == 1) {
    flashWriteData(All_data_flash2);
    writeCheck2 = 0;
  }

  if (writeCheck3 == 1) {
    flashWriteData(All_data_flash3);
    writeCheck3 = 0;
  }

  if (writeCheck4 == 1) {
    flashWriteData(All_data_flash4);
    writeCheck4 = 0;
  }
}

void flashWriteData(String a[10]) {
  if (hafiza == 0) {
    file = SPIFFS.open("/Data.txt", FILE_APPEND);
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else if (hafiza < 9) {
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else {
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    file.close();
    hafiza = 0;
  }
}

void flashRead() {

  readFile(SPIFFS, "/Data.txt");
}

void flashClean() {

  deleteFile(SPIFFS, "/Data.txt");
}
void flashCreatPart() {
  writeFile(SPIFFS, "/Data.txt", All_data_flash1[0]);
}


void listDir(fs::FS &fs, const char *dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.print(file.name());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.print(file.size());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
    }
    file = root.openNextFile();
  }
}

void createDir(fs::FS &fs, const char *path) {
  Serial.printf("Creating Dir: %s\n", path);
  if (fs.mkdir(path)) {
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

void removeDir(fs::FS &fs, const char *path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void readFile(fs::FS &fs, const char *path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return;
  }

  Serial.println("- read from file:");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_WRITE);
  file.print(message);
  file.close();
}

void appendFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_APPEND);
  file.print(message);
  file.close();
}

void deleteFile(fs::FS &fs, const char *path) {
  fs.remove(path);
}
