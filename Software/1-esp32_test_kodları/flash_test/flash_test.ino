#include "FS.h"
#include "FFat.h"

// This file should be compiled with 'Partition Scheme' (in Tools menu)
// set to 'Default with ffat' if you have a 4MB ESP32 dev module or
// set to '16M Fat' if you have a 16MB ESP32 dev module.

// You only need to format FFat the first time you run a test
#define FORMAT_FFAT true



void writeFile(fs::FS &fs, const char * path, const char * message){
    unsigned long start=0;
    unsigned long end=0;
    start=micros();
    File file = fs.open(path, FILE_WRITE);
    end=micros();
    Serial.printf("acma suresi  %d\n",end-start);
    
    start=micros();
    file.print(message);
    end=micros();
    Serial.printf("yazma suresi 1:  %d\n",end-start);
    

    start=micros();
    file.print(message);
    end=micros();
    Serial.printf("yazma suresi 2:  %d\n",end-start);
    

    start=micros();
    file.close();
    end=micros();
    Serial.printf("kapama suresi  %d\n",end-start);
    
}

void appendFile(fs::FS &fs, const char * path, const char * message){
    unsigned long start=0;
    unsigned long end=0;
    start=micros();
    File file = fs.open(path, FILE_APPEND);
    end=micros();
    Serial.printf("acma suresi  %d\n",end-start);
    

    start=micros();
    file.print(message);
    end=micros();
    Serial.printf("yazma suresi 1:  %d\n",end-start);
    

    start=micros();
    file.print(message);
    end=micros();
    Serial.printf("yazma suresi 2:  %d\n",end-start);
    

    start=micros();
    file.close();
    end=micros();
    Serial.printf("kapama suresi  %d\n",end-start);
    
    
}
void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    Serial.printf("Listing directory: %s\r\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        Serial.println("- failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        Serial.println(" - not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if(levels){
                listDir(fs, file.name(), levels -1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("\tSIZE: ");
            Serial.println(file.size());
        }
        file = root.openNextFile();
    }
}
void readFile(fs::FS &fs, const char * path){
    Serial.printf("Reading file: %s\r\n", path);

    File file = fs.open(path);
    if(!file || file.isDirectory()){
        Serial.println("- failed to open file for reading");
        return;
    }

    Serial.println("- read from file:");
    while(file.available()){
        Serial.write(file.read());
    }
    file.close();
}
void renameFile(fs::FS &fs, const char * path1, const char * path2){
    Serial.printf("Renaming file %s to %s\r\n", path1, path2);
    if (fs.rename(path1, path2)) {
        Serial.println("- file renamed");
    } else {
        Serial.println("- rename failed");
    }
}
void deleteFile(fs::FS &fs, const char * path){
    Serial.printf("Deleting file: %s\r\n", path);
    if(fs.remove(path)){
        Serial.println("- file deleted");
    } else {
        Serial.println("- delete failed");
    }
}

void setup(){
    //delay(10000);
    Serial.begin(115200);
    Serial.setDebugOutput(true);
    if (FORMAT_FFAT) FFat.format();
    if(!FFat.begin()){
        Serial.println("FFat Mount Failed");
        return;
    }
    File file;
    Serial.printf("Total space: %10u\n", FFat.totalBytes());
    Serial.printf("Free space: %10u\n", FFat.freeBytes());
    listDir(FFat, "/", 0);
    unsigned long start=0;
    unsigned long end=0;
    char *buf="ulasdasdfsadfsdf\n";
    readFile(FFat, "/hello.txt");
    /*
    start=micros();
    file=FFat.open("/hello.txt",FILE_WRITE);
    file.print(buf);
    file.close();
    end = micros();
    */
    /*
    start=micros();
    writeFile(FFat, "/hello.txt", buf);
    end = micros();
    Serial.printf("acma kapama suresi %d\n",end-start);
    readFile(FFat, "/hello.txt");
    buf="ulasdasdfsadfsdf\n";
    start=micros();
    appendFile(FFat, "/hello.txt", buf);
    end = micros();
    Serial.printf("acma kapama suresi ekleme %d\n",end-start);
    readFile(FFat, "/hello.txt");
    start=micros();
    appendFile(FFat, "/hello.txt", buf);
    end = micros();
    Serial.printf("acma kapama suresi ekleme %d\n",end-start);
    readFile(FFat, "/hello.txt");
    */
    start=micros();
    file = FFat.open("/hello.txt",FILE_WRITE);
    end=micros();
    Serial.printf("acma suresi  %d\n",end-start);
    start=micros();
    file.print(buf);
    end=micros();
    Serial.printf("yazma suresi 1:  %d\n",end-start);
    start=micros();
    file.print(buf);
    end=micros();
    Serial.printf("yazma suresi 2:  %d\n",end-start);
    start=micros();
    file.close();
    end=micros();
    Serial.printf("kapama suresi  %d\n",end-start);
    readFile(FFat, "/hello.txt");

    start=micros();
    file = FFat.open("/hello.txt",FILE_APPEND);
    end=micros();
    Serial.printf("acma suresi  %d\n",end-start);
    start=micros();
    file.print(buf);
    end=micros();
    Serial.printf("yazma suresi 1:  %d\n",end-start);
    start=micros();
    file.print(buf);
    end=micros();
    Serial.printf("yazma suresi 2:  %d\n",end-start);
    start=micros();
    file.close();
    end=micros();
    Serial.printf("kapama suresi  %d\n",end-start);
    readFile(FFat, "/hello.txt");

    start=micros();
    file = FFat.open("/hello.txt",FILE_APPEND);
    end=micros();
    Serial.printf("acma suresi  %d\n",end-start);
    start=micros();
    file.print(buf);
    end=micros();
    Serial.printf("yazma suresi 1:  %d\n",end-start);
    start=micros();
    file.print(buf);
    end=micros();
    Serial.printf("yazma suresi 2:  %d\n",end-start);
    start=micros();
    file.close();
    end=micros();
    Serial.printf("kapama suresi  %d\n",end-start);
    readFile(FFat, "/hello.txt");

    start=micros();
    file = FFat.open("/hello.txt",FILE_APPEND);
    end=micros();
    Serial.printf("acma suresi  %d\n",end-start);
    start=micros();
    file.print(buf);
    end=micros();
    Serial.printf("yazma suresi 1:  %d\n",end-start);
    start=micros();
    file.print(buf);
    end=micros();
    Serial.printf("yazma suresi 2:  %d\n",end-start);
    start=micros();
    file.close();
    end=micros();
    Serial.printf("kapama suresi  %d\n",end-start);
    readFile(FFat, "/hello.txt");

    deleteFile(FFat, "/hello.txt");
    Serial.printf("Free space: %10u\n", FFat.freeBytes());
}

void loop(){

}
