/*
   RadioLib SX126x Ping-Pong Example

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>

// uncomment the following only on one
// of the nodes to initiate the pings
#define INITIATING_NODE

// SX1262 has the following connections:
// NSS pin:   10
// DIO1 pin:  2
// NRST pin:  3
// BUSY pin:  9
SX1262 radio = new Module(15, 4, 32, 33);

// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;

// save transmission states between loops
int transmissionState = ERR_NONE;

// flag to indicate transmission or reception state
bool transmitFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// flag to indicate that a packet was sent or received
volatile bool operationDone = false;

// this function is called when a complete packet
// is transmitted or received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if (!enableInterrupt) {
    return;
  }

  // we sent aor received  packet, set the flag
  operationDone = true;
}
float H3LIS331_data[3] = { 10, 20, 30 };
float Ms5611_data[2] = { 10, 20 };
float bnoAccel[3] = { 10, 20, 30 };
float bnoGyro[3] = { 10, 20, 30 };
float bnoMag[3] = { 10, 20, 30 };
float bnoRaw[3] = { 10, 20, 30 };
long GPS_data[3] = { 10, 20, 30 };
int GPS_time[6] = { 10, 20, 30, 10, 20, 30 };
uint8_t SIV = 3;

struct Data {
  byte byteArray[100];
  int counter = 0;
};
Data data;
void setup() {
  Serial.begin(115200);

  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin();
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }

  // set the function that will be called
  // when new packet is received
  radio.setDio1Action(setFlag);

  // send the first packet on this node
  Serial.print(F("[SX1262] Sending first packet ... "));
  transmissionState = radio.startTransmit("Hello World!");
  transmitFlag = true;

}
int c=0;
int ii;
String All_data1;
byte rxBuffer2[200];
void loop() {
  // check if the previous operation finished
  if (operationDone) {
    // disable the interrupt service routine while
    // processing the data
    enableInterrupt = false;

    // reset flag
    operationDone = false;

    if (transmitFlag) {
      // the previous operation was transmission, listen for response
      // print the result
      if (transmissionState == ERR_NONE) {
        // packet was successfully sent
        Serial.println(F("transmission finished!"));

      } else {
        Serial.print(F("failed, code "));
        Serial.println(transmissionState);
      }

      // listen for response
      radio.startReceive();
      transmitFlag = false;

    } else {
      // the previous operation was reception
      // print data and send another packet
      //int state = radio.readData(rxBuffer2,100);
      int state = radio.readData(data.byteArray,100);

      if (state == ERR_NONE) {
        data.counter=0;
        for( ii=0;ii<3;ii++){ 
        H3LIS331_data[ii]= byteToFloat(data.byteArray,&data.counter);

      }
      for( ii=0;ii<2;ii++){ 
        Ms5611_data[ii]= byteToFloat(data.byteArray,&data.counter);

      }
      for( ii=0;ii<3;ii++){ 
        bnoAccel[ii]= byteToFloat(data.byteArray,&data.counter);

      }
      for( ii=0;ii<3;ii++){ 
        bnoGyro[ii]= byteToFloat(data.byteArray,&data.counter);

      }
      for( ii=0;ii<3;ii++){ 
        bnoMag[ii]= byteToFloat(data.byteArray,&data.counter);

      }
      for( ii=0;ii<3;ii++){ 
        bnoRaw[ii]= byteToFloat(data.byteArray,&data.counter);

      }
      
      // for( ii=0;ii<3;ii++){ 
      //   H3LIS331_data[ii]= ByteToFloat(rxBuffer2,c);
      //   c+=4;
      // }
      // for( ii=0;ii<2;ii++){ 
      //   Ms5611_data[ii]= ByteToFloat(rxBuffer2,c);
      //   c+=4;
      // }
      // for( ii=0;ii<3;ii++){ 
      //   bnoAccel[ii]= ByteToFloat(rxBuffer2,c);
      //   c+=4;
      // }
      // for( ii=0;ii<3;ii++){ 
      //   bnoGyro[ii]= ByteToFloat(rxBuffer2,c);
      // c+=4;
      // }
      // for( ii=0;ii<3;ii++){ 
      //   bnoMag[ii]= ByteToFloat(rxBuffer2,c);
      //   c+=4;
      // }
      // for( ii=0;ii<3;ii++){ 
      //   bnoRaw[ii]= ByteToFloat(rxBuffer2,c);
      //   c+=4;
      // }
      // c=0;
All_data1="X="+String(H3LIS331_data[0])+",Y="+String(H3LIS331_data[1])+",Z="+String(H3LIS331_data[2])+",";
All_data1+=String(Ms5611_data[0])+","+String(Ms5611_data[1])+",";
All_data1+="X="+String(bnoAccel[0])+",Y="+String(bnoAccel[1])+",Z="+String(bnoAccel[2])+",";
All_data1+=String(bnoGyro[0])+","+String(bnoGyro[1])+","+String(bnoGyro[2])+",";
All_data1+=String(bnoMag[0])+","+String(bnoMag[1])+","+String(bnoMag[2])+",";
All_data1+=String(bnoRaw[0])+","+String(bnoRaw[1])+","+String(bnoRaw[2])+"\n";
//All_data1+=String(GPS_time[0])+"-"+String(GPS_time[1])+"-"+String(GPS_time[2])+",";
//All_data1+=String(GPS_time[3])+"."+String(GPS_time[4])+"."+String(GPS_time[5])+"\n";
 Serial.println(All_data1);  

      
      }

      // wait a second before transmitting again
      delay(1000);

      // send another one
      Serial.print(F("[SX1262] Sending another packet ... "));
      transmissionState = radio.startTransmit("Hello World!");
      transmitFlag = true;
    }

    // we're ready to process more packets,
    // enable interrupt service routine
    enableInterrupt = true;
  }
}
uint8_t byteToUint8t(byte *byterray, int *count) {
  uint8_t f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}

float byteToFloat(byte *byterray, int *count) {
  float f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}

long byteToLong(byte *byterray, int *count) {
  long f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt(byte *byterray, int *count) {
  int f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}

byte byteToByte(byte *byterray, int *count) {
  byte f;
  ((uint8_t *)&f)[0] = byterray[*count];
  (*count)++;

  return f;
}




float ByteToFloat(byte *byterray,int ca)
{
  float f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

  long ByteTolong(byte *byterray,int ca)
{
  long f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

    uint8_t ByteTouint8_t(byte *byterray,int ca)
{
  uint8_t f;
  ((uint8_t*)&f)[0]=byterray[ca];
return f;
  }