#include <Wire.h>
#include <Adafruit_H3LIS331.h>
#include <Adafruit_Sensor.h>
#include "MS5611.h"
#include "SparkFun_BNO080_Arduino_Library.h"
#include "SparkFun_Ublox_Arduino_Library.h" //http://librarymanager/All#SparkFun_u-blox_GNSS
#include "FS.h"
#include "SPIFFS.h"


//sensor
Adafruit_H3LIS331 lis = Adafruit_H3LIS331();
SFE_UBLOX_GPS myGPS;
MS5611 MS5611(0x77);   // 0x76 = CSB to VCC; 0x77 = CSB to GND
sensors_event_t event;
BNO080 myIMU;

//sensor data 
float H3LIS331_data[3];
float Ms5611_data[2];
float x_Accel,y_Accel,z_Accel;
float x_Gyro,y_Gyro,z_Gyro;
float x_Mag,y_Mag,z_Mag;
float Roll,Pitch,Yaw;
long GPS_data[3];
int GPS_time[6];
byte SIV;

String All_data="";
String header;
String dosya;
char Pdosya[200]="/Data1.txt";
int hafiza=0;
volatile int count=0; 
int counter=2;
//hafıza
#define FORMAT_SPIFFS_IF_FAILED true
File file;
//timer
hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

//fonksiyon prototipleri
void IRAM_ATTR onTime();

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    Serial.printf("Listing directory: %s\r\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        Serial.println("- failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        Serial.println(" - not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if(levels){
                listDir(fs, file.name(), levels -1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("\tSIZE: ");
            Serial.println(file.size());
        }
        file = root.openNextFile();
    }
}


void readFile(fs::FS &fs, const char * path){
    Serial.printf("Reading file: %s\r\n", path);

    File file = fs.open(path);
    if(!file || file.isDirectory()){
        Serial.println("- failed to open file for reading");
        return;
    }

    Serial.println("- read from file:");
    while(file.available()){
        Serial.write(file.read());
    }
    file.close();
}

void writeFile(fs::FS &fs, const char * path, String message){
    File file = fs.open(path, FILE_WRITE);
    file.print(message);
    file.close();
}

void appendFile(fs::FS &fs, const char * path, String message){
    File file = fs.open(path, FILE_APPEND);
    file.print(message);
    file.close();
}


void deleteFile(fs::FS &fs, const char * path){
    Serial.printf("Deleting file: %s\r\n", path);
    if(fs.remove(path)){
        Serial.println("- file deleted");
    } else {
        Serial.println("- delete failed");
    }
}

void read_sensor();

void setup() {
  Serial.begin(115200);

  //i2c ayar
  Wire.begin();
  Wire.setClock(400000);

  //sensor baslat
  MS5611.begin();
  lis.begin_I2C();   // change this to 0x19 for alternative i2c address
  myIMU.begin();
  myIMU.enableAccelerometer(50); //Send data update every 50ms
  myIMU.enableGyro(50); //Send data update every 50ms
  myIMU.enableMagnetometer(50); //Send data update every 50ms
  myIMU.enableRotationVector(50);
  
  //H3LIS331 ayar
  lis.setRange(H3LIS331_RANGE_100_G);   // 100, 200, or 400 G!
  /*
  Serial.print("Range set to: ");
  switch (lis.getRange()) {
    case H3LIS331_RANGE_100_G: Serial.println("100 g"); break;
    case H3LIS331_RANGE_200_G: Serial.println("200 g"); break;
    case H3LIS331_RANGE_400_G: Serial.println("400 g"); break;
  }
  */
 lis.setDataRate(LIS331_DATARATE_50_HZ);
 /*
  Serial.print("Data rate set to: ");
  switch (lis.getDataRate()) {
    case LIS331_DATARATE_POWERDOWN: Serial.println("Powered Down"); break;
    case LIS331_DATARATE_50_HZ: Serial.println("50 Hz"); break;
    case LIS331_DATARATE_100_HZ: Serial.println("100 Hz"); break;
    case LIS331_DATARATE_400_HZ: Serial.println("400 Hz"); break;
    case LIS331_DATARATE_1000_HZ: Serial.println("1000 Hz"); break;
    case LIS331_DATARATE_LOWPOWER_0_5_HZ: Serial.println("0.5 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_1_HZ: Serial.println("1 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_2_HZ: Serial.println("2 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_5_HZ: Serial.println("5 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_10_HZ: Serial.println("10 Hz Low Power"); break;
  }
*/
  //GPS ayar
  myGPS.begin();
  myGPS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)
  myGPS.setNavigationFrequency(10);
  myGPS.saveConfiguration();        //Save the current settings to flash and BBR

  //hafıza baslat
  SPIFFS.begin(1);
  //readFile(SPIFFS, "/Data.txt");
  for(int p=1;p<50;p++){
  dosya="/Data";
  dosya=dosya+String(p)+".txt";
  int str_len = dosya.length() + 1;
  char char_array[str_len]; 
  dosya.toCharArray(char_array, str_len);
  readFile(SPIFFS, char_array); 
  }
  for(int p=1;p<80;p++){
  dosya="/Data";
  dosya=dosya+String(p)+".txt";
  int str_len = dosya.length() + 1;
  char char_array[str_len]; 
  dosya.toCharArray(char_array, str_len);
  deleteFile(SPIFFS, char_array);
  }
  deleteFile(SPIFFS, "/Data $pp");
  //readFile(SPIFFS, "/Data/Data.txt");
  //deleteFile(SPIFFS, "/Data/Data.txt");
  //createDir(SPIFFS, "/Data");
  //baslık dosyasını yaz
  header="400gAcceloX,400gAcceloY,400gAcceloZ,MS5611Temp,MS5611Pressure,GpsLatitude,GpsLongitude,GpsAltitude,GpsDate,GpsTime\n";
  writeFile(SPIFFS,"/Data1.txt", header);
  
  for(int p=2;p<50;p++){
  dosya="/Data";
  dosya=dosya+String(p)+".txt";
  int str_len = dosya.length() + 1;
  char char_array[str_len]; 
  dosya.toCharArray(char_array, str_len);
  writeFile(SPIFFS, char_array, All_data); 
  Serial.print("."); 
  } 
  
  listDir(SPIFFS, "/", 0);
  dosya="/Data1.txt";
 /* 
  //timer ayar
  timer = timerBegin(0, 80, true);                
  timerAttachInterrupt(timer, &onTime, true);    
  timerAlarmWrite(timer, 20000, true);           
  timerAlarmEnable(timer);
*/
}
unsigned long start=0;
unsigned long end=0;

void loop() {

  if(millis()-start>=20){
    start=millis();
    read_sensor();
    end=millis();
    Serial.printf("okuma suresi: %d \n",end-start);
    //Serial.println(All_data);
  }
 /* 
  if (count > 0) {
    // Comment out enter / exit to deactivate the critical section 
    portENTER_CRITICAL(&timerMux);
    count=0;
    Serial.println("interrupt");
    portEXIT_CRITICAL(&timerMux);
    read_sensor();
    
  }*/
}

//sensor okuma fonksiyonu
void read_sensor(){

  MS5611.read();
  myIMU.dataAvailable();
  lis.getEvent(&event);
  H3LIS331_data[0]=event.acceleration.x/SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[1]=event.acceleration.y/SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[2]=event.acceleration.z/SENSORS_GRAVITY_STANDARD;

  x_Accel = myIMU.getAccelX();
  y_Accel = myIMU.getAccelY();
  z_Accel = myIMU.getAccelZ();
  x_Gyro = myIMU.getGyroX();
  y_Gyro = myIMU.getGyroY();
  z_Gyro = myIMU.getGyroZ();
  x_Mag = myIMU.getMagX();
  y_Mag = myIMU.getMagY();
  z_Mag = myIMU.getMagZ();
  Roll =(myIMU.getRoll()) * 180.0 / PI;
  Pitch = (myIMU.getPitch()) * 180.0 / PI;
  Yaw = (myIMU.getYaw()) * 180.0 / PI;

  Ms5611_data[0]=MS5611.getTemperature();
  Ms5611_data[1]=MS5611.getPressure();

  All_data=String(H3LIS331_data[0]);All_data+=",";
  All_data+=String(H3LIS331_data[1]);All_data+=",";
  All_data+=String(H3LIS331_data[2]);All_data+=",";

  All_data+=String(Ms5611_data[0]);All_data+=",";
  All_data+=String(Ms5611_data[1]);All_data+=",";

  All_data+=String(x_Accel);All_data+=",";
  All_data+=String(y_Accel);All_data+=",";
  All_data+=String(z_Accel);All_data+=",";

  All_data+=String(x_Gyro);All_data+=",";
  All_data+=String(y_Gyro);All_data+=",";
  All_data+=String(z_Gyro);All_data+=",";

  All_data+=String(x_Mag);All_data+=",";
  All_data+=String(y_Mag);All_data+=",";
  All_data+=String(z_Mag);All_data+=",";

  All_data+=String(Roll);All_data+=",";
  All_data+=String(Pitch);All_data+=",";
  All_data+=String(Yaw);All_data+=",";
  //Serial.printf("X: %f,Y: %f,Z: %f g\n",H3LIS331_data[0], H3LIS331_data[1], H3LIS331_data[2]);
  //Serial.printf("T:%.2f,P:%.2f\n",Ms5611_data[0],Ms5611_data[1]);
  //GPS
  /*
  if (i==5)
  {
    unsigned long start1=0;
    unsigned long end1=0;
    i=0;
    start1=micros();
    
    
    //Serial.println("GPS");
    GPS_data[0] = myGPS.getLatitude();
    GPS_data[1] = myGPS.getLongitude();
    GPS_data[2] = myGPS.getAltitude();

    SIV = myGPS.getSIV();

    GPS_time[0]=myGPS.getYear();
    GPS_time[1]=myGPS.getMonth();
    GPS_time[2]=myGPS.getDay();

    GPS_time[3]=myGPS.getHour();
    GPS_time[4]=myGPS.getMinute();
    GPS_time[5]=myGPS.getSecond();

    end1=micros();
    Serial.printf("GPS okuma suresi: %d \n",end1-start1);
    All_data+=String(GPS_data[0]);All_data+=",";
    All_data+=String(GPS_data[1]);All_data+=",";
    All_data+=String(GPS_data[2]);All_data+=",";

    All_data+=String(SIV);All_data+=",";

    All_data+=String(GPS_time[0]);All_data+="-";
    All_data+=String(GPS_time[1]);All_data+="-";
    All_data+=String(GPS_time[2]);All_data+=",";

    All_data+=String(GPS_time[3]);All_data+=".";
    All_data+=String(GPS_time[4]);All_data+=".";
    All_data+=String(GPS_time[5]);All_data+="\n";

    //Serial.printf("Lat: %ld Long: %ld (degrees * 10^-7) Alt: %ld (mm) SIV: %02X \n",GPS_data[0],GPS_data[1],GPS_data[2],(int)SIV);
    //Serial.printf("%d-%02d-%02d %02d:%02d:%02d\n",GPS_time[0],GPS_time[1],GPS_time[2],GPS_time[3],GPS_time[4],GPS_time[5]);
  }
  
  else
  {
    All_data+=",,,,,\n";
  }
  */
  
  All_data+=",,,,,\n";
  
  if(hafiza==0){
    file = SPIFFS.open(Pdosya, FILE_APPEND);
    file.print(All_data);
    Serial.println("acti");
    hafiza++;
  }
  else if(hafiza<100){
    file.print(All_data);
    Serial.println("yazdi");
    hafiza++;
  }
  else{
    
    file.print(All_data);
    file.close();
    hafiza=0;    
    Serial.println("kapadi");
    dosya="/Data";
    dosya=dosya+String(counter)+".txt";
    int str_len = dosya.length() + 1;
    dosya.toCharArray(Pdosya, str_len);  
    counter++;
    All_data="";
    //writeFile(SPIFFS, Pdosya, All_data); 
  }
  
  All_data="";
  //appendFile(SPIFFS, "/Data/Data.txt", All_data);
  //Serial.print(All_data);
}

/*
void IRAM_ATTR onTime() {

  portENTER_CRITICAL_ISR(&timerMux);
  count++;
  portEXIT_CRITICAL_ISR(&timerMux);
  
}
*/
