#include <DFRobot_H3LIS200DL.h>

DFRobot_H3LIS200DL_I2C acce/*(&Wire,0x19)*/;


void setup(void){

  Serial.begin(115200);
  //Chip initialization
  acce.begin();
  
  /**
    set range:Range(g)
              e100_g ,/<±100g>/
              e200_g ,/<±200g>/
  */
  acce.setRange(/*Range = */DFRobot_H3LIS200DL::e100_g);

  /**
   “sleep to wake-up”  need to put the chip in low power mode first
   Set data measurement rate：
   
      ePowerDown_0HZ = 0,
      eLowPower_halfHZ,
      eLowPower_1HZ,
      eLowPower_2HZ,
      eLowPower_5HZ,
      eLowPower_10HZ,
      eNormal_50HZ,
      eNormal_100HZ,
      eNormal_400HZ,
      eNormal_1000HZ,
  */
  acce.setAcquireRate(/*Rate = */DFRobot_H3LIS200DL::eNormal_50HZ);
  
  
}

void loop(void){
    //Get the acceleration in the three directions of xyz
    int start=millis();
    Serial.print("Acceleration x: "); //print acceleration
    Serial.print(acce.readAccX());
    Serial.print(" g \ty: ");
    Serial.print(acce.readAccY());
    Serial.print(" g \tz: ");
    Serial.print(acce.readAccZ());
    Serial.println(" g");
    int end=millis();
    Serial.printf("okuma suresi: %d \n",end-start);
    delay(20);
}