// include the library
#include <RadioLib.h>
#include "SPI.h"
#include "FS.h"
#include "LITTLEFS.h"
#include <Wire.h>
#include <Adafruit_H3LIS331.h>
#include "ms5611_driver.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO08x.h>
#include "SparkFun_Ublox_Arduino_Library.h"
#include <MicroNMEA.h>
#include "ESP32TimerInterrupt.h"
#include "PCA9539.h"
#include "TimeLib.h"
PCA9539 ioport(0x76);

#define SPIFFS LITTLEFS

char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));
SFE_UBLOX_GPS myGPS;
int RXD2 = 25;
int TXD2 = 26;

String fileName[20];
String directoryName[20];
int countFileName = 0;
int countDirecName = 0;

SPIClass SPI2(HSPI);

// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33, SPI2);

int apogee;
int droug;
int carrierFreq = 915;
uint8_t outputPower = 20;
uint8_t loraReSetup = 0;
bool setCal[8];
bool flightMode = 0;
bool flightModeCheck = 0;
bool loraRX = 1;
bool writeMode = 0;
bool parameterStat = 0;
bool flashCleanBit = 0;
bool pyroMode = 0;
bool loraSet = 0;

//sesor dataları
float H3LIS331_data[3];
float Ms5611_data[2];
float bnoAccel[3];
float bnoGyro[3];
float bnoMag[3];
float bnoRaw[3];
long GPS_data[3];
int GPS_time[6];
uint32_t SIV;
tmElements_t my_time;          // time elements structure
unsigned long unix_timestamp;  // a timestamp

uint8_t Led1 = pb3;
uint8_t Led2 = pb4;
uint8_t Pyro1 = pa4;
uint8_t Pyro2 = pa5;

//ESP32Timer ITimer1(1);
int timer100 = 0;
int timer100_2 = 0;
// void IRAM_ATTR TimerHandler1(void) {
//   timer100++;
//   timer100_2++;
// }

struct Data {
  byte byteArray[200];
  int counter = 0;
};
Data data;

struct euler_t {
  float yaw;
  float pitch;
  float roll;
} ypr;

#define BNO08X_CS 5
#define BNO08X_INT 16
#define BNO08X_RESET 2
Adafruit_BNO08x bno08x(BNO08X_RESET);
sh2_SensorValue_t sensorValue;

uint8_t MS_CS = 17;
baro_ms5611 MS5611(MS_CS);

uint8_t accel200gCSPin = 27;
Adafruit_H3LIS331 lis = Adafruit_H3LIS331();

File file;
String All_data1[10];
String All_data2[10];
String All_data3[10];
String All_data4[10];
String All_data_flash1[10];
String All_data_flash2[10];
String All_data_flash3[10];
String All_data_flash4[10];
String header = "400gAcceloX,400gAcceloY,400gAcceloZ,MS5611Temp,MS5611Pressure,BNOAccelX,BNOAccelY,BNOAccelZ,BNOGyroX,BNOGyroY,BNOGyroZ,BNOMagX,BNOMagY,BNOMagZ,Yaw,Pitch,Roll,GpsLatitude,GpsLongitude,GpsAltitude,GpsDate,GpsTime\n";
int hafiza = 0;
int i = 0;

bool flashCheck1 = 0;
bool flashCheck2 = 0;
bool flashCheck3 = 0;
bool flashCheck4 = 0;
bool writeCheck1 = 0;
bool writeCheck2 = 0;
bool writeCheck3 = 0;
bool writeCheck4 = 0;
uint8_t dataCheck = 0;
bool writeStart = 0;

TaskHandle_t Task1;

// save transmission states between loops
int transmissionState = ERR_NONE;

// flag to indicate transmission or reception state
bool transmitFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// flag to indicate that a packet was sent or received
volatile bool operationDone = false;

// this function is called when a complete packet
// is transmitted or received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if (!enableInterrupt) {
    return;
  }

  // we sent aor received  packet, set the flag
  operationDone = true;
}

void setup() {
  Serial.begin(115200);
  setPins();
  //configLora();
  //configFlash();
  configGps();
  configBno();
  configH3lis();
  configMs5611();
  configIOex();

  //ITimer1.attachInterruptInterval(20000, TimerHandler1);
  xTaskCreatePinnedToCore(
    Task1code, /* Task function. */
    "Task1",   /* name of task. */
    10000,     /* Stack size of task */
    NULL,      /* parameter of the task */
    1,         /* priority of the task */
    &Task1,    /* Task handle to keep track of created task */
    0);        /* pin task to core 0 */

  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;
  const int HSPI_CS = 15;

  SPI2.begin(HSPI_SCK, HSPI_MISO, HSPI_MOSI, HSPI_CS);
  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  // carrier frequency:           915.0 MHz
  // bandwidth:                   500.0 kHz
  // spreading factor:            6
  // coding rate:                 5
  // sync word:                   0x34 (public network/LoRaWAN)
  // output power:                2 dBm
  // preamble length:             20 symbols
  int state = radio.begin(carrierFreq, 500.0, 6, 5, 0x34, outputPower);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }

  // set the function that will be called
  // when new packet is received
  radio.setDio1Action(setFlag);
  // start listening for LoRa packets on this node
  Serial.print(F("[SX1262] Starting to listen ... "));
  state = radio.startReceive();
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }
}

void Task1code(void *pvParameters) {
  for (;;) {
    vTaskDelay(1);
    flashWrite();
  }
}
unsigned long start = 0;
void loop() {
  if (!flightMode) {
    if (H3LIS331_data[0] > 2 || H3LIS331_data[1] > 2 || H3LIS331_data[2] > 2 || bnoGyro[0] > 2 || bnoGyro[1] > 2 || bnoGyro[2] > 2 || bnoAccel[0] > 2 || bnoAccel[1] > 2 || bnoAccel[2] > 2) {
      flightMode = 1;
      radio.clearDio1Action();
    } else if (flightModeCheck == 1) {
      if (pyroMode == 0) {
        //uyarı mesajı pyro aktifleştir
        //okuma ile pyro durumlarını bildir
      } else if (writeMode == 0) {
        //write hatası
        flightModeCheck = 0;
      } else if (flashCleanBit == 1) {
        //doluluk uyarısı
        flightModeCheck = 0;
      } else if (parameterStat == 0) {
        //parametre hatası
        flightModeCheck = 0;
      } else if (loraRX == 1) {
        //hersey hazır loranın alıcı modunu kapatabilirsiniz
        flightModeCheck = 0;
      } else {
        flightMode = 1;
        radio.clearDio1Action();
      }
    } else {
      // check if the previous operation finished
      if (operationDone) {
        // disable the interrupt service routine while
        // processing the data
        enableInterrupt = false;

        // reset flag
        operationDone = false;

        if (transmitFlag) {
          // the previous operation was transmission, listen for response
          // print the result
          if (transmissionState == ERR_NONE) {
            // packet was successfully sent
            Serial.println(F("transmission finished!"));

          } else {
            Serial.print(F("failed, code "));
            Serial.println(transmissionState);
          }
          //if (loraRX == 1)
          radio.startReceive();

          transmitFlag = false;

        } else {
          // the previous operation was reception
          // print data and send another packet
          Data data1;
          int state = radio.readData(data.byteArray, 14);

          if (state == ERR_NONE) {
            // packet was successfully received

            Serial.println(F("[SX1262] Received packet!"));
            data.counter = 0;
            // print data of the packet
            Serial.print(F("[SX1262] Data:\t\t"));

            byteToInt(&apogee, &data1.counter, data1.byteArray);
            Serial.println(apogee);
            byteToInt(&droug, &data1.counter, data1.byteArray);
            Serial.println(droug);
            byteToInt(&carrierFreq, &data1.counter, data1.byteArray);
            Serial.println(carrierFreq);
            byteToInt8(&outputPower, &data1.counter, data1.byteArray);
            Serial.println(outputPower);
            byteToInt8(&loraReSetup, &data1.counter, data1.byteArray);
            Serial.println(loraReSetup);
            for (int i = 0; loraReSetup > 0; i++) {
              setCal[i] = loraReSetup % 2;
              loraReSetup = loraReSetup / 2;
            }
            flightModeCheck = setCal[0];
            loraRX = setCal[1];
            writeMode = setCal[2];
            parameterStat = setCal[3];
            flashCleanBit = setCal[4];
            pyroMode = setCal[5];
            loraSet = setCal[6];

            if (loraSet) {
              loraSet = 0;
              radio.setFrequency(carrierFreq);
              radio.setOutputPower(outputPower);
            }
            if (flashCleanBit) {
              flashCleanBit = 0;
              cleanFlash();
            }
          }
          // wait a second before transmitting again
          delay(1000);

          // send another one
          Serial.print(F("[SX1262] Sending another packet ... "));
          transmissionState = radio.startTransmit("Hello World!");
          transmitFlag = true;
        }

        // we're ready to process more packets,
        // enable interrupt service routine
        enableInterrupt = true;
      }
    }
  } else {
    delay(1);
    readBno();
    if (millis() - start >= 100) {
      start = millis();
      readH3lis();
      readMs5611();
      sendDataLora();
      readGPS();
      writeData();
    }
  }
}

void setPins() {
  pinMode(BNO08X_CS, 1);
  pinMode(accel200gCSPin, 1);
  pinMode(MS_CS, 1);
  digitalWrite(BNO08X_CS, 1);
  digitalWrite(accel200gCSPin, 1);
  digitalWrite(MS_CS, 1);
}

void configLora() {
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin(868.0, 500.0, 7, 5, 0x34, 20);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }
}

void configFlash() {
  SPIFFS.begin(1);
  //flashRead();
  flashClean();
  //baslık dosyasını yaz
  writeFile(SPIFFS, "/Data.txt", header);
  flashCreatPart();
}

void configGps() {
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  myGPS.begin(Serial2);
  myGPS.setSerialRate(38400);
  Serial.println("Trying 38400 baud");
  Serial2.begin(38400, SERIAL_8N1, RXD2, TXD2);
  myGPS.begin(Serial2);
  //This will pipe all NMEA sentences to the serial port so we can see them
  myGPS.setNMEAOutputPort(Serial);
  myGPS.setNavigationFrequency(10);
}

void SFE_UBLOX_GPS::processNMEA(char incoming) {
  //Take the incoming char from the Ublox I2C port and pass it on to the MicroNMEA lib
  //for sentence cracking
  nmea.process(incoming);
}

void configBno() {
  Serial.println("Adafruit BNO08x test!");
  if (!bno08x.begin_SPI(BNO08X_CS, BNO08X_INT)) {
    Serial.println("Failed to find BNO08x chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("BNO08x Found!");

  setReports();
  Serial.println("Reading events");
  delay(100);
}

// Here is where you define the sensor outputs you want to receive
void setReports(void) {
  Serial.println("Setting desired reports");
  if (!bno08x.enableReport(SH2_ACCELEROMETER)) {
    Serial.println("Could not enable accelerometer");
  }
  if (!bno08x.enableReport(SH2_GYROSCOPE_CALIBRATED)) {
    Serial.println("Could not enable gyroscope");
  }
  if (!bno08x.enableReport(SH2_MAGNETIC_FIELD_CALIBRATED)) {
    Serial.println("Could not enable magnetic field calibrated");
  }
  if (!bno08x.enableReport(SH2_ROTATION_VECTOR)) {
    Serial.println("Could not enable rotation vector");
  }
}

void quaternionToEuler(float qr, float qi, float qj, float qk, euler_t *ypr, bool degrees = false) {

  float sqr = sq(qr);
  float sqi = sq(qi);
  float sqj = sq(qj);
  float sqk = sq(qk);

  ypr->yaw = atan2(2.0 * (qi * qj + qk * qr), (sqi - sqj - sqk + sqr));
  ypr->pitch = asin(-2.0 * (qi * qk - qj * qr) / (sqi + sqj + sqk + sqr));
  ypr->roll = atan2(2.0 * (qj * qk + qi * qr), (-sqi - sqj + sqk + sqr));

  if (degrees) {
    ypr->yaw *= RAD_TO_DEG;
    ypr->pitch *= RAD_TO_DEG;
    ypr->roll *= RAD_TO_DEG;
  }
}

void configH3lis() {
  if (!lis.begin_SPI(accel200gCSPin)) {
    Serial.println("Couldnt start");
    while (1) yield();
  }
  lis.setRange(H3LIS331_RANGE_100_G);  // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_50_HZ);
}

void configMs5611() {
  MS5611.Initialize();
}

void configIOex() {
  Wire.begin(21, 22);
  ioport.pinMode(Pyro1, OUTPUT);
  ioport.pinMode(Pyro2, OUTPUT);
  ioport.digitalWrite(Pyro1, LOW);
  ioport.digitalWrite(Pyro2, LOW);
  delay(100);

  ioport.pinMode(Led1, OUTPUT);
  ioport.pinMode(Led2, OUTPUT);
  ioport.digitalWrite(Led1, LOW);
  ioport.digitalWrite(Led2, LOW);
  delay(100);
}

void readBno() {
  if (!bno08x.getSensorEvent(&sensorValue)) {
    return;
  }

  switch (sensorValue.sensorId) {
    case SH2_ACCELEROMETER:
      bnoAccel[0] = (sensorValue.un.accelerometer.x);
      bnoAccel[1] = (sensorValue.un.accelerometer.y);
      bnoAccel[2] = (sensorValue.un.accelerometer.z);
      break;
    case SH2_GYROSCOPE_CALIBRATED:
      bnoGyro[0] = (sensorValue.un.gyroscope.x);
      bnoGyro[1] = (sensorValue.un.gyroscope.y);
      bnoGyro[2] = (sensorValue.un.gyroscope.z);
      break;
    case SH2_MAGNETIC_FIELD_CALIBRATED:
      bnoMag[0] = (sensorValue.un.magneticField.x);
      bnoMag[1] = (sensorValue.un.magneticField.y);
      bnoMag[2] = (sensorValue.un.magneticField.z);
      break;
    case SH2_ROTATION_VECTOR:
      quaternionToEuler(sensorValue.un.rotationVector.real, sensorValue.un.rotationVector.i, sensorValue.un.rotationVector.j, sensorValue.un.rotationVector.k, &ypr, true);
      bnoRaw[0] = (ypr.yaw);
      bnoRaw[1] = (ypr.pitch);
      bnoRaw[2] = (ypr.roll);
      break;
  }
}

void readH3lis() {
  sensors_event_t event;
  lis.getEvent(&event);
  H3LIS331_data[0] = event.acceleration.x / SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[1] = event.acceleration.y / SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[2] = event.acceleration.z / SENSORS_GRAVITY_STANDARD;
  digitalWrite(accel200gCSPin, 1);
}

void readMs5611() {
  MS5611.updateTemperature();
  MS5611.getPressure();
  Ms5611_data[0] = MS5611.getTemperature_degC();
  Ms5611_data[1] = MS5611.getPressure_mbar();
}

void byteToFloat(float *data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&data)[i] = array[*count];
    (*count)++;
  }
}

void byteToInt(int *data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&data)[i] = array[*count];
    (*count)++;
  }
}

void byteToInt16(uint16_t *data, int *count, byte *array) {
  for (int i = 0; i < 2; i++) {
    ((uint8_t *)&data)[i] = array[*count];
    (*count)++;
  }
}

void byteToInt8(uint8_t *data, int *count, byte *array) {
  ((uint8_t *)&data)[0] = array[*count];
  (*count)++;
}

void floatAddByte(float data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}

void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}

void toByte() {
  for (int k = 0; k < 3; k++) {
    floatAddByte(H3LIS331_data[k], &data.counter, data.byteArray);
  }

  for (int k = 0; k < 2; k++) {
    floatAddByte(Ms5611_data[k], &data.counter, data.byteArray);
  }

  for (int k = 0; k < 3; k++) {
    floatAddByte(bnoAccel[k], &data.counter, data.byteArray);
  }
  for (int k = 0; k < 3; k++) {
    floatAddByte(bnoGyro[k], &data.counter, data.byteArray);
  }
  for (int k = 0; k < 3; k++) {
    floatAddByte(bnoMag[k], &data.counter, data.byteArray);
  }
  for (int k = 0; k < 3; k++) {
    floatAddByte(bnoRaw[k], &data.counter, data.byteArray);
  }

  for (int k = 0; k < 3; k++) {
    longAddByte(GPS_data[k], &data.counter, data.byteArray);
  }
  byteAddByte(SIV, &data.counter, data.byteArray);
  longAddByte(unix_timestamp, &data.counter, data.byteArray);
  data.counter = 0;
}

void sendDataLora() {
  toByte();
  int state = radio.transmit(data.byteArray, 89);

  if (state == ERR_NONE) {
    // the packet was successfully transmitted
    Serial.println(F("success!"));
    // print measured data rate
    // Serial.print(F("[SX1262] Datarate:\t"));
    // Serial.print(radio.getDataRate());
    // Serial.println(F(" bps"));

  } else if (state == ERR_PACKET_TOO_LONG) {
    // the supplied packet was longer than 256 bytes
    Serial.println(F("too long!"));

  } else if (state == ERR_TX_TIMEOUT) {
    // timeout occured while transmitting packet
    Serial.println(F("timeout!"));

  } else {
    // some other error occurred
    Serial.print(F("failed, code "));
    Serial.println(state);
  }
}

void readGPS() {
  myGPS.checkUblox();  //See if new data is available. Process bytes as they come in.
  if (nmea.isValid() == true) {
    GPS_data[0] = nmea.getLatitude();
    GPS_data[1] = nmea.getLongitude();
    nmea.getAltitude(GPS_data[2]);
    SIV = nmea.getNumSatellites();
    GPS_time[0] = nmea.getYear();
    GPS_time[1] = nmea.getMonth();
    GPS_time[2] = nmea.getDay();
    GPS_time[3] = nmea.getHour();
    GPS_time[4] = nmea.getMinute();
    GPS_time[5] = nmea.getSecond();
    my_time.Year = GPS_time[0] - 1970;
    my_time.Month = GPS_time[1];
    my_time.Day = GPS_time[2];
    my_time.Hour = GPS_time[3];
    my_time.Minute = GPS_time[4];
    my_time.Second = GPS_time[5];
    unix_timestamp = (long)(makeTime(my_time));
  }
}

void writeData() {

  if (dataCheck == 0) {
    while (flashCheck1) {
      vTaskDelay(1);
    }
    addDataString(All_data1[i]);
    //Serial.println(All_data1[i]);
    if (i == 9) {
      i = 0;
      flashCheck1 = 1;
      dataCheck = 1;
    } else {
      i++;
    }
  } else if (dataCheck == 1) {
    while (flashCheck2) {
      vTaskDelay(1);
    }
    addDataString(All_data2[i]);
    if (i == 9) {
      i = 0;
      flashCheck2 = 1;
      dataCheck = 2;
    } else {
      i++;
    }
  } else if (dataCheck == 2) {
    while (flashCheck3) {
      vTaskDelay(1);
    }
    addDataString(All_data3[i]);
    if (i == 9) {
      i = 0;
      flashCheck3 = 1;
      dataCheck = 3;
    } else {
      i++;
    }
  } else if (dataCheck == 3) {
    while (flashCheck4) {
      vTaskDelay(1);
    }
    addDataString(All_data4[i]);
    if (i == 9) {
      i = 0;
      flashCheck4 = 1;
      dataCheck = 0;
    } else {
      i++;
    }
  }
}

void addDataString(String &Data) {
  Data = String(H3LIS331_data[0]) + "," + String(H3LIS331_data[1]) + "," + String(H3LIS331_data[2]) + ",";
  Data += String(Ms5611_data[0]) + "," + String(Ms5611_data[1]) + ",";
  Data += String(bnoAccel[0]) + "," + String(bnoAccel[1]) + "," + String(bnoAccel[2]) + ",";
  Data += String(bnoGyro[0]) + "," + String(bnoGyro[1]) + "," + String(bnoGyro[2]) + ",";
  Data += String(bnoMag[0]) + "," + String(bnoMag[1]) + "," + String(bnoMag[2]) + ",";
  Data += String(bnoRaw[0]) + "," + String(bnoRaw[1]) + "," + String(bnoRaw[2]) + ",";
  Data += String(GPS_data[0]) + "," + String(GPS_data[1]) + "," + String(GPS_data[2]) + ",";
  Data += String(SIV) + ",";
  Data += String(GPS_time[0]) + "-" + String(GPS_time[1]) + "-" + String(GPS_time[2]) + ",";
  Data += String(GPS_time[3]) + "." + String(GPS_time[4]) + "." + String(GPS_time[5]) + "\n";
}

void flashWrite() {

  if (flashCheck1 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash1[j] = All_data1[j];
    }
    flashCheck1 = 0;
    writeCheck1 = 1;
  }

  if (flashCheck2 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash2[j] = All_data2[j];
    }
    flashCheck2 = 0;
    writeCheck2 = 1;
  }

  if (flashCheck3 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash3[j] = All_data3[j];
    }
    flashCheck3 = 0;
    writeCheck3 = 1;
  }

  if (flashCheck4 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash4[j] = All_data4[j];
    }
    flashCheck4 = 0;
    writeCheck4 = 1;
  }

  if (writeCheck1 == 1) {
    flashWriteData(All_data_flash1);
    writeCheck1 = 0;
  }

  if (writeCheck2 == 1) {
    flashWriteData(All_data_flash2);
    writeCheck2 = 0;
  }

  if (writeCheck3 == 1) {
    flashWriteData(All_data_flash3);
    writeCheck3 = 0;
  }

  if (writeCheck4 == 1) {
    flashWriteData(All_data_flash4);
    writeCheck4 = 0;
  }
}

void flashWriteData(String a[10]) {
  if (hafiza == 0) {
    file = SPIFFS.open("/Data.txt", FILE_APPEND);
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else if (hafiza < 9) {
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else {
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    file.close();
    hafiza = 0;
  }
}

void flashRead() {
  readFile(SPIFFS, "/Data.txt");
}

void flashClean() {

  deleteFile(SPIFFS, "/Data.txt");
}

void flashCreatPart() {
  writeFile(SPIFFS, "/Data.txt", All_data_flash1[0]);
}

void removeDir(fs::FS &fs, const char *path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void readFile(fs::FS &fs, const char *path) {
  Serial.printf("Reading file: %s\n", path);

  File file = fs.open(path);
  if (!file) {
    Serial.println("Failed to open file for reading");
    return;
  }

  Serial.print("Read from file: ");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char *path, String message) {
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if (!file) {
    Serial.println("Failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

void appendFile(fs::FS &fs, const char *path, const char *message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if (!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if (file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}

void deleteFile(fs::FS &fs, const char *path) {
  Serial.printf("Deleting file: %s\n", path);
  if (fs.remove(path)) {
    Serial.println("File deleted");
  } else {
    Serial.println("Delete failed");
  }
}

void getDir(fs::FS &fs, const char *dirname, uint8_t levels) {

  File root = fs.open(dirname);
  File file = root.openNextFile();

  while (file) {
    if (file.isDirectory()) {
      directoryName[countDirecName] = String(file.name());
      countDirecName++;
      if (levels) {
        getDir(fs, file.name(), levels - 1);
      }
    } else {
      fileName[countFileName] = String(file.name());
      ++countFileName;
    }
    file = root.openNextFile();
  }
}

void deleteDir() {
  for (int j = 0; j < countFileName; j++) {
    int str_len = fileName[j].length() + 1;
    char char_array[str_len];
    fileName[j].toCharArray(char_array, str_len);
    deleteFile(SPIFFS, char_array);
  }

  for (int i = 0; i < countDirecName; i++) {
    int str_len = directoryName[i].length() + 1;
    char char_array[str_len];
    directoryName[i].toCharArray(char_array, str_len);
    removeDir(SPIFFS, char_array);
  }
}

void cleanFlash() {
  getDir(SPIFFS, "/", 1);
  deleteDir();
}