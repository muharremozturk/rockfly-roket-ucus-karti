// Basic demo for readings from Adafruit BNO08x
#include <Adafruit_BNO08x.h>
#include <Adafruit_H3LIS331.h>
#include <Adafruit_Sensor.h>


Adafruit_H3LIS331 lis = Adafruit_H3LIS331();
// For SPI mode, we need a CS pin
#define BNO08X_CS 5
#define BNO08X_INT 25
#define BNO08X_RESET 14

Adafruit_BNO08x bno08x(BNO08X_RESET);
sh2_SensorValue_t sensorValue;

uint8_t MS_CS = 2;
uint8_t accel200gCSPin = 17;

int sampleTime = 0;
 void setup(void) {
  pinMode(accel200gCSPin, 1);
  
  pinMode(MS_CS, 1);
  digitalWrite(accel200gCSPin, 1);
  digitalWrite(MS_CS, 1);

  Serial.begin(115200);
  while (!Serial)
    delay(10);  // will pause Zero, Leonardo, etc until serial console opens

  Serial.println("Adafruit BNO08x test!");
  if (!bno08x.begin_SPI(BNO08X_CS, BNO08X_INT)) {
    Serial.println("Failed to find BNO08x chip");
    //while (1) {
      delay(10);
    }
  }
  Serial.println("BNO08x Found!");

  setReports();
  Serial.println("Reading events");
  delay(100);

  if (!lis.begin_SPI(accel200gCSPin)) {
    Serial.println("Couldnt start");
    while (1) yield();
  }
  Serial.println("H3LIS331 found!");

  lis.setRange(H3LIS331_RANGE_100_G);  // 100, 200, or 400 G!
  Serial.print("Range set to: ");
  switch (lis.getRange()) {
    case H3LIS331_RANGE_100_G: Serial.println("100 g"); break;
    case H3LIS331_RANGE_200_G: Serial.println("200 g"); break;
    case H3LIS331_RANGE_400_G: Serial.println("400 g"); break;
  }
  lis.setDataRate(LIS331_DATARATE_100_HZ);
  Serial.print("Data rate set to: ");
  switch (lis.getDataRate()) {

    case LIS331_DATARATE_POWERDOWN: Serial.println("Powered Down"); break;
    case LIS331_DATARATE_50_HZ: Serial.println("50 Hz"); break;
    case LIS331_DATARATE_100_HZ: Serial.println("100 Hz"); break;
    case LIS331_DATARATE_400_HZ: Serial.println("400 Hz"); break;
    case LIS331_DATARATE_1000_HZ: Serial.println("1000 Hz"); break;
    case LIS331_DATARATE_LOWPOWER_0_5_HZ: Serial.println("0.5 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_1_HZ: Serial.println("1 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_2_HZ: Serial.println("2 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_5_HZ: Serial.println("5 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_10_HZ: Serial.println("10 Hz Low Power"); break;
  }
  //lis.setHPFReference();
  lis.enableHighPassFilter(1,LIS331_HPF_0_0025_ODR);
  lis.setLPFCutoff(LIS331_LPF_37_HZ);
}

// Here is where you define the sensor outputs you want to receive
void setReports(void) {
  Serial.println("Setting desired reports");
  if (!bno08x.enableReport(SH2_LINEAR_ACCELERATION, 10)) {
    Serial.println("Could not enable accelerometer");
  }
}

float bnoAccel[3] = { 0, 0, 0 };
float h3200gAccel[3] = { 0, 0, 0 };
void loop() {
  readSensor(5);
  printSensor();
}
sensors_event_t event;
void readSensor(int sample) {
  for (int i = 0; i < 3; i++) {
    bnoAccel[i] = 0;
    h3200gAccel[i] = 0;
  }
  sampleTime = millis();
  int sampleCount = 0;
  int lastTime=0;
  while (sampleCount < sample) {
    if(millis()-lastTime>2){
    if (!bno08x.getSensorEvent(&sensorValue)) {
      
      
      return;
    }
    switch (sensorValue.sensorId) {
      case SH2_LINEAR_ACCELERATION:
      lis.getEvent(&event);
        bnoAccel[0] += sensorValue.un.linearAcceleration.x / 9.80665;
        bnoAccel[1] += sensorValue.un.linearAcceleration.y / 9.80665;
        bnoAccel[2] += sensorValue.un.linearAcceleration.z / 9.80665;

        h3200gAccel[0] += -event.acceleration.y / 9.80665;
        h3200gAccel[1] += event.acceleration.x / 9.80665;
        h3200gAccel[2] += event.acceleration.z / 9.80665;
        sampleCount++;
        break;
    }
    lastTime=millis();
    }
  }
  sampleCount = 0;
  for (int i = 0; i < 3; i++) {
    bnoAccel[i] = bnoAccel[i] / sample;
    h3200gAccel[i] = h3200gAccel[i] / sample;
  }
  sampleTime = millis() - sampleTime;
}
void printSensor() {
  Serial.print("bno - x: ");
  Serial.print(bnoAccel[0]);
  Serial.print(", y:  ");
  Serial.print(bnoAccel[1]);
  Serial.print(",z: ");
  Serial.print(bnoAccel[2]);
  Serial.print(",200g - X: ");
  Serial.print(h3200gAccel[0]);
  Serial.print(",Y: ");
  Serial.print(h3200gAccel[1]);
  Serial.print(",Z: ");
  Serial.print(h3200gAccel[2]);
  Serial.print(",Sample Time ");
  Serial.println(sampleTime);
}