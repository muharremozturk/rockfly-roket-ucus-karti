#include "FS.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_BNO08x.h>
#include <Adafruit_H3LIS331.h>
#include <Adafruit_Sensor.h>
#include "LITTLEFS.h"
#include <MicroNMEA.h>
#include "ms5611_spi.h"
#include "PCA9539.h"
#include <RadioLib.h>
#include "SparkFun_Ublox_Arduino_Library.h"  
#include "TimeLib.h"
#include <Vector.h>
#include <WiFi.h>              // Built-in
#include <WebServer.h>

#define ServerVersion "1.0"
String  webpage = "";
#define SPIFFS LITTLEFS
bool    SPIFFS_present = false;
const char* ssid = "RockFLY2022";
const char* password = "rockfly2022";

// Variables to save values from HTML form
String apooge;
String drouge;
String direction;

WebServer server(80);


const int ELEMENT_COUNT_MAX = 50;
typedef Vector<String> Elements;
String storage_array[ELEMENT_COUNT_MAX];
Elements vector;

char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));

PCA9539 ioport(0x76);

SFE_UBLOX_GPS myGPS;
int RXD2 = 26;
int TXD2 = 27;

#define buzzer 12
#define pyro1 5
#define pyro2 4
#define pyro1S 39
#define pyro2S 36
#define led1 11
#define led2 12
#define batt 35


// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33);

int transmissionState = ERR_NONE;
bool transmitFlag = false;
volatile bool enableInterrupt = true;
volatile bool operationDone = false;
void setFlag(void) {
  if (!enableInterrupt) {
    return;
  }
  operationDone = true;
}

byte passw = 0x60;
byte errorData = 0x00;
byte setConfig = 0x10;
byte sensData = 0x20;
byte flightSensData = 0x21;  // uçuştaki datalar
byte flightSensStatData = 0x22;
byte sendTouchGrndData = 0x23;
byte getConfigData = 0x30;
byte getPrshtCon = 0x50;
byte sendFrqConfig = 0x31;
byte sendPrshtCon = 0x51;

float mainPar=10;
float loraFreq = 868.00;
uint8_t outputPower = 20;
uint8_t pyroStat = 0;
bool pyroStatArr[8];

bool flightMode = 1;

float speed;
float altitude;
float altitudeOld;
int altitudeTimer;
bool pyroFire1 = 0;
bool pyroFire2 = 0;
bool pyroProt1 = 0;
bool pyroProt2 = 0;
bool machLock = 0;
bool groundMode = 0;
bool apfallSpeedStart = 0;
bool apfallSpeedStop = 0;
unsigned int apfallSpeedCount = 0;
bool mainfallSpeedStart = 0;
bool mainfallSpeedStop = 0;
unsigned int mainfallSpeedCount = 0;

struct flightData {
  float H3LIS331_data[3];
  float Ms5611_data[4];
  float bnoAccel[3];
  float bnoGyro[3];
  float bnoMag[3];
  float bnoRaw[3];
  long GPS_data[4];
  int GPS_time[6];
  byte SIV = 0;
  tmElements_t my_time;    // time elements structure
  unsigned long unixTime;  // a timestamp
  float battery;

  float maxAltitude;
  float maxSpeed;
  float maxAccel[3];
  byte maxSiv;
  unsigned long engineBurnTime;
  unsigned long apogeeTime;
  float apogeeFallSpeed;
  float mainFallSpeed;
  unsigned long descendTime;

  unsigned long flightTime;
};
flightData flightdata;


struct datas {
  byte byteArray[200];
  int counter = 0;
};
datas data;

struct euler_t {
  float yaw;
  float pitch;
  float roll;
} ypr;

#define BNO08X_CS 5
#define BNO08X_INT 25
#define BNO08X_RESET 14
Adafruit_BNO08x bno08x(BNO08X_RESET);
sh2_SensorValue_t sensorValue;

uint8_t MS_CS = 2;
baro_ms5611 MS5611(MS_CS);

uint8_t accel200gCSPin = 17;
Adafruit_H3LIS331 lis = Adafruit_H3LIS331();

File file;
String All_data1[10];
String All_data2[10];
String All_data3[10];
String All_data4[10];
String All_data_flash1[10];
String All_data_flash2[10];
String All_data_flash3[10];
String All_data_flash4[10];
String header = "200gAccX,200gAccY,200gAccZ,Temp,Press,Alt,Speed,AccX,AccY,AccZ,GyrX,GyrY,GyrZ,MagX,MagY,MagZ,Yaw,Pitch,Roll,GpsLat,GpsLong,GpsAlt,SIV,Batt,PyroStat,GpsTime\n";
String headerStat ="maxAltitude,maxSpeed,maxAccelX,maxAccelY,maxAccelZ,maxSiv,engineBurnTime,apogeeTime,apogeeFallSpeed,mainFallSpeed,descendTime\n";
String statData;
int hafiza = 0;
int i = 0;

//#define SPIFFS LITTLEFS

bool flashCheck1 = 0;
bool flashCheck2 = 0;
bool flashCheck3 = 0;
bool flashCheck4 = 0;
bool writeCheck1 = 0;
bool writeCheck2 = 0;
bool writeCheck3 = 0;
bool writeCheck4 = 0;
uint8_t dataCheck = 0;
bool writeStart = 0;
bool error = 0;
bool sendSensorData = 0;
bool send_frq_ConfigData = 0;
bool setConOk = 0;
bool sendPrchtConData = 0;
bool writeMode = 1;
bool storageArrayWrite = 0;
bool setGpsFreq = 0;
bool sendgroundData = 0;
bool sendStat = 0;
bool configFlashCheck=0;
bool writeStatCheck = 0;
TaskHandle_t Task1;

int pushButton = 0;
unsigned long wifiModeTimer = 0;

void setup() {
  Serial.begin(115200);
  setPins();
  configLora();
  configGps();
  configBno();
  configH3lis();
  configMs5611();
  configFlash();
  buzzerToggle(5,500);
  wifiMode();
  
  xTaskCreatePinnedToCore(
    Task1code, /* Task function. */
    "Task1",   /* name of task. */
    10000,     /* Stack size of task */
    NULL,      /* parameter of the task */
    1,         /* priority of the task */
    &Task1,    /* Task handle to keep track of created task */
    0);        /* pin task to core 0 */

  
}

void Task1code(void *pvParameters) {

  for (;;) {
    vTaskDelay(1);
    if (writeMode) {
      if(!configFlashCheck){
        appendFile(SPIFFS, "/Data.txt", header);
        configFlashCheck=1;
      } 
      flashWrite();
      if(writeStatCheck){
        writeStat();
      }
    }
  }

}

unsigned long start = 0;
unsigned long start2 = 0;
void loop() {
  delay(1);
  if (!flightMode == 1) {
    if (!groundMode) readBno();
    if (millis() - start >= 99) {
      start = millis();
      readGPS();
      if (!groundMode) {
        readMs5611();
        String data;
        addDataString(data);
        if (vector.size() == vector.max_size()) {
          vector.remove(0);
        }
        vector.push_back(data);
      }
    }
    
    if (altitude > 8 && ((-1.5 > flightdata.bnoAccel[0] || flightdata.bnoAccel[0] > 1.5) || (-1.5 > flightdata.bnoAccel[1] || flightdata.bnoAccel[1] > 1.5) || (-1.5 > flightdata.bnoAccel[2] || flightdata.bnoAccel[2] > 1.5))) {
      Serial.println("ucus modu");
      radio.clearDio1Action();
      flightMode = 1;
      writeMode = 1;
      storageArrayWrite = 1;
      //buzzerToggle(5,100);
      flightdata.apogeeTime = millis();
    } else if (operationDone) {
      enableInterrupt = false;
      operationDone = false;
      if (transmitFlag) {
        if (transmissionState == ERR_NONE) {
          Serial.println(F("transmission finished!"));
        } else {
          Serial.print(F("failed, code "));
          Serial.println(transmissionState);
        }
        radio.startReceive();
        transmitFlag = false;

      } else {
        int state = radio.readData(data.byteArray, 15);
        if (state == ERR_NONE) {
          Serial.println(F("[SX1262] Received packet!"));
          if (data.byteArray[0] == passw && data.byteArray[1] == getConfigData) {
            error = 0;
            send_frq_ConfigData = 1;
            data.counter = 2;
            loraFreq = byteToFloat(data.byteArray, &data.counter);
            outputPower = byteToUint8t(data.byteArray, &data.counter);
          } else if (data.byteArray[0] == passw && data.byteArray[1] == sendFrqConfig) {
            error = 0;
            send_frq_ConfigData = 1;
          } else if (data.byteArray[0] == passw && data.byteArray[1] == sendPrshtCon) {
            error = 0;
            sendPrchtConData = 1;
          } else if (data.byteArray[0] == passw && data.byteArray[1] == setConfig) {
            error = 0;
            setConOk = 1;
            radio.setFrequency(loraFreq);
            radio.setOutputPower(outputPower);
          } else if (data.byteArray[0] == passw && data.byteArray[1] == getPrshtCon) {
            error = 0;
            sendPrchtConData = 1;
            data.counter = 2;
            mainPar = byteToFloat(data.byteArray, &data.counter);
            
          } else if (data.byteArray[0] == passw && data.byteArray[1] == sensData) {
            sendSensorData = 1;
          } else if (data.byteArray[0] == passw && data.byteArray[1] == sendTouchGrndData) {
            sendgroundData = 1;
          } else {
            error = 1;
          }
        }
        Serial.print(F("[SX1262] Sending another packet ... "));
        if (error == 0 && send_frq_ConfigData == 1) {
          send_frq_ConfigData = 0;
        
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sendFrqConfig, &data.counter, data.byteArray);
          floatAddByte(loraFreq, &data.counter, data.byteArray);
          uint8AddByte(outputPower, &data.counter, data.byteArray);
          delay(100);
          transmissionState = radio.startTransmit(data.byteArray, 7);
        } else if (error == 0 && sendPrchtConData == 1) {
          sendPrchtConData = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sendPrshtCon, &data.counter, data.byteArray);
          floatAddByte(mainPar, &data.counter, data.byteArray);
          delay(100);
          transmissionState = radio.startTransmit(data.byteArray, 10);
        } else if (error == 0 && setConOk == 1) {
          setConOk = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(setConfig, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray, 2);
        } else if (error == 0 && sendSensorData == 1) {
          sendSensorData = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sensData, &data.counter, data.byteArray);
          toByteAndStat();
          transmissionState = radio.startTransmit(data.byteArray, 100);
        } else if (error == 0 && sendgroundData == 1) {
          sendgroundData = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sendTouchGrndData, &data.counter, data.byteArray);
          for (int k = 0; k < 3; k++) longAddByte(flightdata.GPS_data[k], &data.counter, data.byteArray);
          byteAddByte(flightdata.SIV, &data.counter, data.byteArray);
          floatAddByte(flightdata.battery, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray, 20);
        } else {
          error = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          //hatali mesaj
          byteAddByte(errorData, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray, 15);
        }

        transmitFlag = true;
      }
      enableInterrupt = true;
    }
  }else {
    start2=millis();
    //ucus kodları
    if (altitude > 20 && !pyroProt1) {
      pyroProt1 = 1;
      Serial.println("pyroProt1");
    }

    if (pyroProt1 && !machLock && !pyroProt2) {
      if (millis() - altitudeTimer >= 3000) {
        
        if (altitude - altitudeOld < -5) {
          Serial.println("pyro1Fire");
          pyro1Fire();
          pyroProt2 = 1;
          apfallSpeedStart = 1;
          sendStat = 1;
          flightdata.apogeeTime = millis() - flightdata.apogeeTime;
          flightdata.descendTime = millis();

        }
        altitudeOld = altitude;
        altitudeTimer = millis();
      }
    }

    if (apfallSpeedStart) {
      flightdata.apogeeFallSpeed += speed;
      apfallSpeedCount++;
      if (apfallSpeedStop) {
        flightdata.apogeeFallSpeed = flightdata.apogeeFallSpeed / (apfallSpeedCount + 1);
        apfallSpeedStart = 0;
      }
    }
        
    if (!groundMode && pyroProt2 && (altitude - mainPar) < 10) {
       Serial.println((altitude - mainPar));
      Serial.println("groundMode");
      apfallSpeedStop = 1;
      mainfallSpeedStart = 1;
      pyro2Fire();
      groundMode = 1;
    }

    if (groundMode) {
      if (millis() - altitudeTimer >= 5000) {

        if ((altitude - altitudeOld > -2)&& ((-0.2 < flightdata.bnoGyro[0] || flightdata.bnoGyro[0] < 0.2) || (-0.2 < flightdata.bnoGyro[1] || flightdata.bnoGyro[1] < 0.2) || (-0.2 < flightdata.bnoGyro[2] || flightdata.bnoGyro[2] < 0.2))) {
          Serial.println("flightMode = 0");
          pyroProt1 = 0;
          pyroProt2 = 0;
          mainfallSpeedStop = 1;
          flightdata.mainFallSpeed = flightdata.mainFallSpeed / (mainfallSpeedCount + 1);
          mainfallSpeedStart = 0;
          flightdata.descendTime = millis() - flightdata.descendTime;
          flightdata.flightTime = flightdata.descendTime + flightdata.apogeeTime;
          statData=String(flightdata.maxAltitude)+","+String(flightdata.maxSpeed)+","+
          String(flightdata.maxAccel[0])+","+String(flightdata.maxAccel[1])+","+String(flightdata.maxAccel[2])+","+
          String(flightdata.maxSiv)+","+String(flightdata.engineBurnTime)+","+String(flightdata.apogeeTime)+","+
          String(flightdata.apogeeFallSpeed)+","+String(flightdata.mainFallSpeed)+","+String(flightdata.descendTime)+"\n";
          writeStatCheck=1;
          while(writeStatCheck) delay(1);
          //flightMode = 0;
          //writeMode = 0;
          //delay(100);
          //configLora();
        }
        altitudeOld = altitude;
        altitudeTimer = millis();
      }
    }

    if (mainfallSpeedStart) {
      flightdata.mainFallSpeed += speed;
      mainfallSpeedCount++;
    }
    
    float Gama = 1.4;
    float R = 287.05;
    float T = flightdata.Ms5611_data[0] + 273;  //Kelvine dönüştü
    //mach lock
    if (sqrt(Gama * R * T) <= (speed / 0.93969)) {
      machLock = 1;
    } else
      machLock = 0;

    readBno();
    if (millis() - start >= 99) {
      start=millis();
      readMs5611();
      readGPS();
      readVoltage();
      sendDataLora();
      writeData();
    }
    if(millis()-start2>100){
      Serial.println(millis()-start2);
    }
    
  }
}

void setPins() {
  configIOex();
  pinMode(BNO08X_CS, OUTPUT);
  pinMode(accel200gCSPin, OUTPUT);
  pinMode(MS_CS, OUTPUT);
  pinMode(buzzer, OUTPUT);
  digitalWrite(BNO08X_CS, 1);
  digitalWrite(accel200gCSPin, 1);
  digitalWrite(MS_CS, 1);
  digitalWrite(buzzer, 0);
  pinMode(pushButton, INPUT_PULLUP);
}

void configLora() {
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin(loraFreq, 500.0, 7, 5, 0x34, outputPower);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }
  radio.setDio1Action(setFlag);
  Serial.print(F("[SX1262] Starting to listen ... "));
  state = radio.startReceive();
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }
}

void configFlash() {
  if(!SPIFFS.begin(1)){
    Serial.println("LITTLEFS Mount Failed");
    while(1);
  }
  //flashRead();
  //flashClean();
  flashCreatPart();
  //baslık dosyasını yaz
  vector.setStorage(storage_array);
 
}

void configGps() {
  do {
    Serial.println("GNSS: trying 38400 baud");
    Serial2.begin(38400, SERIAL_8N1, RXD2, TXD2);
    if (myGPS.begin(Serial2) == true) break;

    delay(100);
    Serial.println("GNSS: trying 9600 baud");
    Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
    if (myGPS.begin(Serial2) == true) {
      Serial.println("GNSS: connected at 9600 baud, switching to 38400");
      myGPS.setSerialRate(38400);
      delay(100);
    } else {
      //myGNSS.factoryReset();
      delay(2000);  //Wait a bit before trying again to limit the Serial output
    }
  } while (1);
  Serial.println("GNSS serial connected");

  myGPS.setNMEAOutputPort(Serial);
  myGPS.saveConfiguration();  //Save the current settings to flash and BBR
  myGPS.setNavigationFrequency(10);
}


void configBno() {
  Serial.println("Adafruit BNO08x test!");
  if (!bno08x.begin_SPI(BNO08X_CS, BNO08X_INT)) {
    Serial.println("Failed to find BNO08x chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("BNO08x Found!");

  setReports();
  Serial.println("Reading events");
  delay(100);
}

// Here is where you define the sensor outputs you want to receive
void setReports(void) {
  Serial.println("Setting desired reports");
  if (!bno08x.enableReport(SH2_ACCELEROMETER, 10)) {
    Serial.println("Could not enable accelerometer");
  }
  if (!bno08x.enableReport(SH2_GYROSCOPE_CALIBRATED, 10)) {
    Serial.println("Could not enable gyroscope");
  }
  if (!bno08x.enableReport(SH2_MAGNETIC_FIELD_CALIBRATED, 10)) {
    Serial.println("Could not enable magnetic field calibrated");
  }
  if (!bno08x.enableReport(SH2_ROTATION_VECTOR, 10)) {
    Serial.println("Could not enable rotation vector");
  }
}

void quaternionToEuler(float qr, float qi, float qj, float qk, euler_t *ypr, bool degrees = false) {

  float sqr = sq(qr);
  float sqi = sq(qi);
  float sqj = sq(qj);
  float sqk = sq(qk);

  ypr->yaw = atan2(2.0 * (qi * qj + qk * qr), (sqi - sqj - sqk + sqr));
  ypr->pitch = asin(-2.0 * (qi * qk - qj * qr) / (sqi + sqj + sqk + sqr));
  ypr->roll = atan2(2.0 * (qj * qk + qi * qr), (-sqi - sqj + sqk + sqr));

  if (degrees) {
    ypr->yaw *= RAD_TO_DEG;
    ypr->pitch *= RAD_TO_DEG;
    ypr->roll *= RAD_TO_DEG;
  }
}

void configH3lis() {
  if (!lis.begin_SPI(accel200gCSPin)) {
    Serial.println("Couldnt start");
    while (1) yield();
  }
  lis.setRange(H3LIS331_RANGE_100_G);  // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_100_HZ);
  lis.enableHighPassFilter(1, LIS331_HPF_0_0025_ODR);
  lis.setLPFCutoff(LIS331_LPF_37_HZ);
}

void configMs5611() {
  MS5611.initialize();
  MS5611.calibrateAltitude();
}

void configIOex() {
  Wire.begin(21, 22);
  Wire.setClock(400000);
  ioport.pinMode(pyro1, OUTPUT);
  ioport.pinMode(pyro2, OUTPUT);
  ioport.pinMode(led1, OUTPUT);
  ioport.pinMode(led2, OUTPUT);
  delay(100);
}

void readBno() {
  if (!bno08x.getSensorEvent(&sensorValue)) {
    return;
  }

  switch (sensorValue.sensorId) {

    case SH2_ACCELEROMETER:
      readH3lis();
      flightdata.bnoAccel[0] = (sensorValue.un.accelerometer.x) / SENSORS_GRAVITY_STANDARD;
      flightdata.bnoAccel[1] = (sensorValue.un.accelerometer.y) / SENSORS_GRAVITY_STANDARD;
      flightdata.bnoAccel[2] = (sensorValue.un.accelerometer.z) / SENSORS_GRAVITY_STANDARD;
      break;
    case SH2_GYROSCOPE_CALIBRATED:
      flightdata.bnoGyro[0] = (sensorValue.un.gyroscope.x);
      flightdata.bnoGyro[1] = (sensorValue.un.gyroscope.y);
      flightdata.bnoGyro[2] = (sensorValue.un.gyroscope.z);
      break;
    case SH2_MAGNETIC_FIELD_CALIBRATED:
      flightdata.bnoMag[0] = (sensorValue.un.magneticField.x);
      flightdata.bnoMag[1] = (sensorValue.un.magneticField.y);
      flightdata.bnoMag[2] = (sensorValue.un.magneticField.z);
      break;
    case SH2_ROTATION_VECTOR:
      quaternionToEuler(sensorValue.un.rotationVector.real, sensorValue.un.rotationVector.i, sensorValue.un.rotationVector.j, sensorValue.un.rotationVector.k, &ypr, true);
      flightdata.bnoRaw[0] = (ypr.yaw);
      flightdata.bnoRaw[1] = (ypr.pitch);
      flightdata.bnoRaw[2] = (ypr.roll);
      break;

  }
}

void readH3lis() {

  sensors_event_t event;
  lis.getEvent(&event);
  flightdata.H3LIS331_data[0] = event.acceleration.x / SENSORS_GRAVITY_STANDARD;
  flightdata.H3LIS331_data[1] = event.acceleration.y / SENSORS_GRAVITY_STANDARD;
  flightdata.H3LIS331_data[2] = event.acceleration.z / SENSORS_GRAVITY_STANDARD;
  digitalWrite(accel200gCSPin, 1);

}

void readMs5611() {

  MS5611.updateData();
  MS5611.updateCalAltitudeKalman();
  MS5611.updateSpeedKalman();
  flightdata.Ms5611_data[0] = MS5611.getTemperature_degC();
  flightdata.Ms5611_data[1] = MS5611.getPressure_mbar();
  flightdata.Ms5611_data[2] = MS5611.getAltitudeKalman();
  altitude = flightdata.Ms5611_data[2];
  flightdata.Ms5611_data[3] = MS5611.getVelMs();
  speed = flightdata.Ms5611_data[3];

}
uint8_t byteToUint8t(byte *byterray, int *count) {
  uint8_t f;
  ((uint8_t *)&f)[i] = byterray[*count];
  return f;
}

float byteToFloat(byte *byterray, int *count) {
  float f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}

long byteToLong(byte *byterray, int *count) {
  long f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
unsigned long byteToUnsignedLong(byte *byterray, int *count) {
  unsigned long f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt(byte *byterray, int *count) {
  int f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
byte byteToByte(byte *byterray, int *count) {
  byte f;
  ((uint8_t *)&f)[0] = byterray[*count];
  (*count)++;
  return f;
}
void floatAddByte(float data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void longAddByte(long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void unsignedLongAddByte(unsigned long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}
void uint8AddByte(uint8_t data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}
float maxAccel[3];
float maxSpeed;
float maxAltitude;
byte maxSiv;
void toByteAndStat() {

  for (int k = 0; k < 3; k++) {
    floatAddByte(flightdata.H3LIS331_data[k], &data.counter, data.byteArray);
    if (abs(flightdata.H3LIS331_data[k]) > abs(flightdata.maxAccel[k])) flightdata.maxAccel[k] = flightdata.H3LIS331_data[k];
  }

  //for (int k = 0; k < 2; k++) floatAddByte(flightdata.Ms5611_data[k], &data.counter, data.byteArray);

  for (int k = 0; k < 4; k++) floatAddByte(flightdata.Ms5611_data[k], &data.counter, data.byteArray);

  if (flightdata.Ms5611_data[2] > flightdata.maxAltitude) flightdata.maxAltitude = flightdata.Ms5611_data[2];
  if (flightdata.Ms5611_data[3] > flightdata.maxSpeed) flightdata.maxSpeed = flightdata.Ms5611_data[3];

  for (int k = 0; k < 3; k++) {
    floatAddByte(flightdata.bnoAccel[k], &data.counter, data.byteArray);
    if (abs(flightdata.bnoAccel[k]) > abs(flightdata.maxAccel[k])) flightdata.maxAccel[k] = flightdata.bnoAccel[k];
  }

  for (int k = 0; k < 3; k++) floatAddByte(flightdata.bnoGyro[k], &data.counter, data.byteArray);

  for (int k = 0; k < 3; k++) floatAddByte(flightdata.bnoMag[k], &data.counter, data.byteArray);

  for (int k = 0; k < 3; k++) floatAddByte(flightdata.bnoRaw[k], &data.counter, data.byteArray);

  for (int k = 0; k < 4; k++) longAddByte(flightdata.GPS_data[k], &data.counter, data.byteArray);

  byteAddByte(flightdata.SIV, &data.counter, data.byteArray);
  if (flightdata.SIV > flightdata.maxSiv) flightdata.maxSiv = flightdata.SIV;

  byteAddByte(pyroStat, &data.counter, data.byteArray);

  floatAddByte(flightdata.battery, &data.counter, data.byteArray);

  longAddByte(flightdata.unixTime, &data.counter, data.byteArray);
}

void toByteStat() {

  floatAddByte(flightdata.maxAltitude, &data.counter, data.byteArray);
  floatAddByte(flightdata.maxSpeed, &data.counter, data.byteArray);
  for (int k = 0; k < 3; k++) floatAddByte(flightdata.maxAccel[k], &data.counter, data.byteArray);
  byteAddByte(flightdata.maxSiv, &data.counter, data.byteArray);
  unsignedLongAddByte(flightdata.engineBurnTime, &data.counter, data.byteArray);
  unsignedLongAddByte(flightdata.apogeeTime, &data.counter, data.byteArray);
  if(apfallSpeedStop) floatAddByte(flightdata.apogeeFallSpeed, &data.counter, data.byteArray);
  else floatAddByte(0, &data.counter, data.byteArray);
  if(mainfallSpeedStop){
  floatAddByte(flightdata.mainFallSpeed, &data.counter, data.byteArray);
  unsignedLongAddByte(flightdata.descendTime, &data.counter, data.byteArray);
  }
  else {
    floatAddByte(0, &data.counter, data.byteArray);
    floatAddByte(0, &data.counter, data.byteArray);
  }
}

void sendDataLora() {
  int size=100;
  data.counter = 0;
  byteAddByte(passw, &data.counter, data.byteArray);
  if(sendStat){
    byteAddByte(flightSensStatData, &data.counter, data.byteArray);
  }
  else{
    byteAddByte(flightSensData, &data.counter, data.byteArray);
  }
  
  toByteAndStat();
  if (sendStat) {
    toByteStat();
    size=150;
  }
  else{
    size=100;
  }
  
  int state = radio.transmit(data.byteArray, size);
  
  if (state == ERR_NONE) {

  } else if (state == ERR_PACKET_TOO_LONG) {
    // the supplied packet was longer than 256 bytes
    Serial.println(F("too long!"));

  } else if (state == ERR_TX_TIMEOUT) {
    // timeout occured while transmitting packetgps
    Serial.println(F("timeout!"));

  } else {
    // some other error occurred
    Serial.print(F("failed, code "));
    Serial.println(state);
  }
  
}
int gpsCount = 0;
/*
void readGPS() {
  if (gpsCount >= 9) {
    myGPS.checkUblox();  //See if new data is available. Process bytes as they come in.
    if (nmea.isValid() == true || flightdata.SIV >= 4) {
      flightdata.GPS_data[0] = nmea.getLatitude();
      flightdata.GPS_data[1] = nmea.getLongitude();
      nmea.getAltitude(flightdata.GPS_data[2]);
      flightdata.SIV = nmea.getNumSatellites();
      flightdata.GPS_time[0] = nmea.getYear();
      flightdata.GPS_time[1] = nmea.getMonth();
      flightdata.GPS_time[2] = nmea.getDay();
      flightdata.GPS_time[3] = nmea.getHour();
      flightdata.GPS_time[4] = nmea.getMinute();
      flightdata.GPS_time[5] = nmea.getSecond();
      flightdata.my_time.Year = flightdata.GPS_time[0] - 1970;
      flightdata.my_time.Month = flightdata.GPS_time[1];
      flightdata.my_time.Day = flightdata.GPS_time[2];
      flightdata.my_time.Hour = flightdata.GPS_time[3];
      flightdata.my_time.Minute = flightdata.GPS_time[4];
      flightdata.my_time.Second = flightdata.GPS_time[5];
      flightdata.unixTime = (long)(makeTime(flightdata.my_time));
      gpsCount = 10;
    } else if (4 > flightdata.SIV >= 1) {
      flightdata.SIV = nmea.getNumSatellites();
      flightdata.GPS_time[0] = nmea.getYear();
      flightdata.GPS_time[1] = nmea.getMonth();
      flightdata.GPS_time[2] = nmea.getDay();
      flightdata.GPS_time[3] = nmea.getHour();
      flightdata.GPS_time[4] = nmea.getMinute();
      flightdata.GPS_time[5] = nmea.getSecond();
      flightdata.my_time.Year = flightdata.GPS_time[0] - 1970;
      flightdata.my_time.Month = flightdata.GPS_time[1];
      flightdata.my_time.Day = flightdata.GPS_time[2];
      flightdata.my_time.Hour = flightdata.GPS_time[3];
      flightdata.my_time.Minute = flightdata.GPS_time[4];
      flightdata.my_time.Second = flightdata.GPS_time[5];
      flightdata.unixTime = (long)(makeTime(flightdata.my_time));
      gpsCount = 0;
      if (flightdata.SIV >= 4 && !setGpsFreq) {
        setGpsFreq = 1;
        myGPS.setNavigationFrequency(10);
        gpsCount = 10;
      }
    } else {
      flightdata.SIV = nmea.getNumSatellites();
      gpsCount = 0;
      if (flightdata.SIV >= 4 && !setGpsFreq) {
        setGpsFreq = 1;
        myGPS.setNavigationFrequency(10);
        gpsCount = 10;
      }
    }
  } else {
    gpsCount++;
  }
}
*/
void readGPS() {

  myGPS.checkUblox();  //See if new data is available. Process bytes as they come in.
  if (nmea.isValid() == true) {
    flightdata.GPS_data[0] = nmea.getLatitude();
    flightdata.GPS_data[1] = nmea.getLongitude();
    nmea.getAltitude(flightdata.GPS_data[2]);
    flightdata.GPS_data[3] = nmea.getSpeed();
    flightdata.SIV = nmea.getNumSatellites();
    flightdata.GPS_time[0] = nmea.getYear();
    flightdata.GPS_time[1] = nmea.getMonth();
    flightdata.GPS_time[2] = nmea.getDay();
    flightdata.GPS_time[3] = nmea.getHour();
    flightdata.GPS_time[4] = nmea.getMinute();
    flightdata.GPS_time[5] = nmea.getSecond();
    flightdata.my_time.Year = flightdata.GPS_time[0] - 1970;
    flightdata.my_time.Month = flightdata.GPS_time[1];
    flightdata.my_time.Day = flightdata.GPS_time[2];
    flightdata.my_time.Hour = flightdata.GPS_time[3];
    flightdata.my_time.Minute = flightdata.GPS_time[4];
    flightdata.my_time.Second = flightdata.GPS_time[5];
    flightdata.unixTime = (long)(makeTime(flightdata.my_time));
  }
}
void SFE_UBLOX_GPS::processNMEA(char incoming) {
  //Take the incoming char from the Ublox I2C port and pass it on to the MicroNMEA lib
  //for sentence cracking
  nmea.process(incoming);
}

void readVoltage() {
  flightdata.battery = readBatt();
  pyroStatArr[0] = pyro1Read();
  pyroStatArr[1] = pyro2Read();
  pyroStatArr[2] = pyroFire1;
  pyroStatArr[3] = pyroFire2;
  pyroStatArr[4] = groundMode;
  pyroStatArr[5] = mainfallSpeedStop;
  pyroStatArr[6] = machLock;
  pyroStat = 0;
  for (int i = 0; i < 7; i++) {
    pyroStat += pyroStatArr[i] * pow(2, i);
  }
}

void pyro1Fire() {
  if (pyroProt1) {
    ioport.digitalWrite(pyro1, HIGH);
    pyroFire1 = 1;
  }
}
void pyro1Off() {
  ioport.digitalWrite(pyro1, LOW);
}

bool pyro1Read() {
  if ((analogRead(pyro1S) * 3.3 / 4095) * 6.6 > 2)
    return 1;
  else
    return 0;
}

void pyro2Fire() {
  if (pyroProt2) {
    ioport.digitalWrite(pyro2, HIGH);
    pyroFire2 = 1;
  }
}
void pyro2Off() {
  ioport.digitalWrite(pyro2, LOW);
}
bool pyro2Read() {
  if ((analogRead(pyro2S) * 3.3 / 4095) * 6.6 > 2)
    return 1;
  else
    return 0;
}
void led1On() {
  ioport.digitalWrite(led1, HIGH);
}
void led1Off() {
  ioport.digitalWrite(led1, LOW);
}
void led2On() {
  ioport.digitalWrite(led2, HIGH);
}
void led2Off() {
  ioport.digitalWrite(led2, LOW);
}

void buzzerToggle(int loop,int delayms) {
  for(int p=0;p<loop;p++){
    digitalWrite(buzzer, HIGH);
    delay(delayms);
    digitalWrite(buzzer, LOW);
    delay(delayms);
  }
}

double readBatt() {
  double reading = analogRead(batt);  // Reference voltage is 3v3 so maximum reading is 3v3 = 4095 in range 0 to 4095
  if (reading < 1 || reading > 4095) return 0;
  // return -0.000000000009824 * pow(reading,3) + 0.000000016557283 * pow(reading,2) + 0.000854596860691 * reading + 0.065440348345433;
  return (-0.000000000000016 * pow(reading, 4) + 0.000000000118171 * pow(reading, 3) - 0.000000301211691 * pow(reading, 2) + 0.001109019271794 * reading + 0.034143524634089) * 6.6;
}

void writeData() {

  if (dataCheck == 0) {
    while (flashCheck1) {
      vTaskDelay(1);
    }
    addDataString(All_data1[i]);
    //Serial.println(All_data1[i]);
    if (i == 9) {
      i = 0;
      flashCheck1 = 1;
      dataCheck = 1;
    } else {
      i++;
    }
  } else if (dataCheck == 1) {
    while (flashCheck2) {
      vTaskDelay(1);
    }
    addDataString(All_data2[i]);
    if (i == 9) {
      i = 0;
      flashCheck2 = 1;
      dataCheck = 2;
    } else {
      i++;
    }
  } else if (dataCheck == 2) {
    while (flashCheck3) {
      vTaskDelay(1);
    }
    addDataString(All_data3[i]);
    if (i == 9) {
      i = 0;
      flashCheck3 = 1;
      dataCheck = 3;
    } else {
      i++;
    }
  } else if (dataCheck == 3) {
    while (flashCheck4) {
      vTaskDelay(1);
    }
    addDataString(All_data4[i]);
    if (i == 9) {
      i = 0;
      flashCheck4 = 1;
      dataCheck = 0;
    } else {
      i++;
    }
  }
}

void addDataString(String &Data) {
  Data = String(flightdata.H3LIS331_data[0]) + "," + String(flightdata.H3LIS331_data[1]) + "," + String(flightdata.H3LIS331_data[2]) + ",";
  Data += String(flightdata.Ms5611_data[0]) + "," + String(flightdata.Ms5611_data[1]) + "," + String(flightdata.Ms5611_data[2]) + "," + String(flightdata.Ms5611_data[3]) + ",";
  Data += String(flightdata.bnoAccel[0]) + "," + String(flightdata.bnoAccel[1]) + "," + String(flightdata.bnoAccel[2]) + ",";
  Data += String(flightdata.bnoGyro[0]) + "," + String(flightdata.bnoGyro[1]) + "," + String(flightdata.bnoGyro[2]) + ",";
  Data += String(flightdata.bnoMag[0]) + "," + String(flightdata.bnoMag[1]) + "," + String(flightdata.bnoMag[2]) + ",";
  Data += String(flightdata.bnoRaw[0]) + "," + String(flightdata.bnoRaw[1]) + "," + String(flightdata.bnoRaw[2]) + ",";
  Data += String(flightdata.GPS_data[0]) + "," + String(flightdata.GPS_data[1]) + "," + String(flightdata.GPS_data[2]) + "," + String(flightdata.GPS_data[3]) + ",";
  Data += String(flightdata.SIV) +"," +  String(flightdata.battery) +"," +  String(pyroStat)+ "," + String(flightdata.unixTime) + "\n";
}

void flashWrite() {

  if (storageArrayWrite) {
    storageArrayWrite = 0;
    flashWriteFirstData(storage_array);
  }
  if (flashCheck1 == 1) {
    for (int j = 0; j < 10; j++) {
      All_data_flash1[j] = All_data1[j];
    }
    flashCheck1 = 0;
    writeCheck1 = 1;
  }

  if (flashCheck2 == 1) {
    for (int j = 0; j < 10; j++) {
      All_data_flash2[j] = All_data2[j];
    }
    flashCheck2 = 0;
    writeCheck2 = 1;
  }

  if (flashCheck3 == 1) {
    for (int j = 0; j < 10; j++) {
      All_data_flash3[j] = All_data3[j];
    }
    flashCheck3 = 0;
    writeCheck3 = 1;
  }

  if (flashCheck4 == 1) {
    for (int j = 0; j < 10; j++) {
      All_data_flash4[j] = All_data4[j];
    }
    flashCheck4 = 0;
    writeCheck4 = 1;
  }

  if (writeCheck1 == 1) {
    flashWriteData(All_data_flash1);
    writeCheck1 = 0;
  }

  if (writeCheck2 == 1) {
    flashWriteData(All_data_flash2);
    writeCheck2 = 0;
  }

  if (writeCheck3 == 1) {
    flashWriteData(All_data_flash3);
    writeCheck3 = 0;
  }

  if (writeCheck4 == 1) {
    flashWriteData(All_data_flash4);
    writeCheck4 = 0;
  }

}

void writeStat(){
  appendFile(SPIFFS, "/Data.txt", headerStat);
  appendFile(SPIFFS, "/Data.txt", statData);
  writeStatCheck=0;
}

void flashWriteFirstData(String a[ELEMENT_COUNT_MAX]) {
  file = SPIFFS.open("/Data.txt", FILE_APPEND);
  for (int j = 0; j < ELEMENT_COUNT_MAX; j++) {
    file.print(a[j]);
  }
  file.close();
}
void flashWriteData(String a[10]) {
  if (hafiza == 0) {
    file = SPIFFS.open("/Data.txt", FILE_APPEND);
    for (int j = 0; j < 10; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else if (hafiza < 9) {
    for (int j = 0; j < 10; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else {
    for (int j = 0; j < 10; j++) {
      file.print(a[j]);
    }
    file.close();
    hafiza = 0;
  }
}

void flashRead() {
  readFile(SPIFFS, "/Data.txt");
}

void flashClean() {

  deleteFile(SPIFFS, "/Data.txt");
}
void flashCreatPart() {
  appendFile(SPIFFS, "/Data.txt", All_data_flash1[0]);
}


void listDir(fs::FS &fs, const char *dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.print(file.name());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.print(file.size());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
    }
    file = root.openNextFile();
  }
}

void createDir(fs::FS &fs, const char *path) {
  Serial.printf("Creating Dir: %s\n", path);
  if (fs.mkdir(path)) {
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

void removeDir(fs::FS &fs, const char *path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void readFile(fs::FS &fs, const char *path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return;
  }

  Serial.println("- read from file:");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_WRITE);
  file.print(message);
  file.close();
}

void appendFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_APPEND);
  file.print(message);
  file.close();
}

void deleteFile(fs::FS &fs, const char *path) {
  fs.remove(path);
}
void wifiMode(){
  if(!digitalRead(pushButton)){
    Serial.println("buton");
    wifiModeTimer=millis();
    while(!digitalRead(pushButton)){
      if(millis()-wifiModeTimer>2000){
        //buzzerToggle(2,500);
        led1On();
        led2On();
        writeMode = 0;
        WiFi.softAP(ssid, password);
        IPAddress IP = WiFi.softAPIP();
        Serial.print("AP IP address: ");
        Serial.println(IP);  

        if (!SPIFFS.begin(true)) {
        Serial.println("SPIFFS initialisation failed...");
        SPIFFS_present = false; 
        }
        else
        {
          Serial.println(F("SPIFFS initialised... file access enabled..."));
          SPIFFS_present = true; 
        }
        //----------------------------------------------------------------------   
        ///////////////////////////// Server Commands 
        server.on("/",         HomePage);
        server.on("/download", File_Download);
        server.on("/upload",   File_Upload);
        server.on("/fupload",  HTTP_POST,[](){ server.send(200);}, handleFileUpload);
        server.on("/delete",   File_Delete);
        server.on("/data",     Data);
        server.on("/settings", Settings);
        server.begin();
        Serial.println("HTTP server started");
        while(1) server.handleClient();
      }
    }
  }
}
void append_page_header() {
  webpage  = F("<!DOCTYPE html><html>");
  webpage += F("<head>");
  webpage += F("<title>File Server</title>"); // NOTE: 1em = 16px
  webpage += F("<meta name='viewport' content='user-scalable=yes,initial-scale=1.0,width=device-width'>");
  webpage += F("<style>");
  webpage += F("body{max-width:65%;margin:0 auto;font-family:arial;font-size:105%;text-align:center;color:blue;background-color:#F7F2Fd;}");
  webpage += F("ul{list-style-type:none;margin:0.1em;padding:0;border-radius:0.375em;overflow:hidden;background-color:#dcade6;font-size:1em;}");
  webpage += F("li{float:left;border-radius:0.375em;border-right:0.06em solid #bbb;}last-child {border-right:none;font-size:85%}");
  webpage += F("li a{display: block;border-radius:0.375em;padding:0.44em 0.44em;text-decoration:none;font-size:85%}");
  webpage += F("li a:hover{background-color:#EAE3EA;border-radius:0.375em;font-size:85%}");
  webpage += F("section {font-size:0.88em;}");
  webpage += F("h1{color:white;border-radius:0.5em;font-size:1em;padding:0.2em 0.2em;background:#558ED5;}");
  webpage += F("h2{color:orange;font-size:1.0em;}");
  webpage += F("h3{font-size:0.8em;}");
  webpage += F("table{font-family:arial,sans-serif;font-size:0.9em;border-collapse:collapse;width:85%;}"); 
  webpage += F("th,td {border:0.06em solid #dddddd;text-align:left;padding:0.3em;border-bottom:0.06em solid #dddddd;}"); 
  webpage += F("tr:nth-child(odd) {background-color:#eeeeee;}");
  webpage += F(".rcorners_n {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:20%;color:white;font-size:75%;}");
  webpage += F(".rcorners_m {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:50%;color:white;font-size:75%;}");
  webpage += F(".rcorners_w {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:70%;color:white;font-size:75%;}");
  webpage += F(".column{float:left;width:50%;height:45%;}");
  webpage += F(".row:after{content:'';display:table;clear:both;}");
  webpage += F("*{box-sizing:border-box;}");
  webpage += F("footer{background-color:#eedfff; text-align:center;padding:0.3em 0.3em;border-radius:0.375em;font-size:60%;}");
  webpage += F("button{border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:20%;color:white;font-size:130%;}");
  webpage += F(".buttons {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:15%;color:white;font-size:80%;}");
  webpage += F(".buttonsm{border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:9%; color:white;font-size:70%;}");
  webpage += F(".buttonm {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:15%;color:white;font-size:70%;}");
  webpage += F(".buttonw {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:40%;color:white;font-size:70%;}");
  webpage += F("a{font-size:75%;}");
  webpage += F("p{font-size:75%;}");
  webpage += F("</style></head><body><h1>File Server "); webpage += String(ServerVersion) + "</h1>";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void append_page_footer(){ // Saves repeating many lines of code for HTML page footers
  webpage += F("<ul>");
  webpage += F("<li><a href='/'>Home</a></li>"); // Lower Menu bar command entries
  webpage += F("<li><a href='/download'>Download</a></li>"); 
  webpage += F("<li><a href='/upload'>Upload</a></li>"); 
  webpage += F("<li><a href='/delete'>Delete</a></li>"); 
  webpage += F("<li><a href='/data'>Data</a></li>");
  webpage += F("<li><a href='/settings'>Data</a></li>");
  webpage += F("</ul>");
  webpage += "<footer>&copy;"+String(char(byte(0x40>>1)))+String(char(byte(0x88>>1)))+String(char(byte(0x5c>>1)))+String(char(byte(0x98>>1)))+String(char(byte(0x5c>>1)));
  webpage += String(char((0x84>>1)))+String(char(byte(0xd2>>1)))+String(char(0xe4>>1))+String(char(0xc8>>1))+String(char(byte(0x40>>1)));
  webpage += String(char(byte(0x64/2)))+String(char(byte(0x60>>1)))+String(char(byte(0x62>>1)))+String(char(0x70>>1))+"</footer>";
  webpage += F("</body></html>");
}

void HomePage(){
  SendHTML_Header();
  webpage += F("<a href='/download'><button>Download</button></a>");
  webpage += F("<a href='/upload'><button>Upload</button></a>");
  webpage += F("<a href='/delete'><button>Delete</button></a>");
  webpage += F("<a href='/data'><button>Data</button></a>");
  webpage += F("<a href='/settings'><button>Settings</button></a>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop(); // Stop is needed because no content length was sent
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void File_Download(){ // This gets called twice, the first pass selects the input, the second pass then processes the command line arguments
  if (server.args() > 0 ) { // Arguments were received
    if (server.hasArg("download")) DownloadFile(server.arg(0));
  }
  else SelectInput("Enter filename to download","download","download");
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void DownloadFile(String filename){
  if (SPIFFS_present) { 
    File download = SPIFFS.open("/"+filename,  "r");
    if (download) {
      server.sendHeader("Content-Type", "text/text");
      server.sendHeader("Content-Disposition", "attachment; filename="+filename);
      server.sendHeader("Connection", "close");
      server.streamFile(download, "application/octet-stream");
      download.close();
    } else ReportFileNotPresent("download"); 
  } else ReportSPIFFSNotPresent();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void File_Upload(){
  append_page_header();
  webpage += F("<h3>Select File to Upload</h3>"); 
  webpage += F("<FORM action='/fupload' method='post' enctype='multipart/form-data'>");
  webpage += F("<input class='buttons' style='width:40%' type='file' name='fupload' id = 'fupload' value=''><br>");
  webpage += F("<br><button class='buttons' style='width:10%' type='submit'>Upload File</button><br>");
  webpage += F("<a href='/'>[Back]</a><br><br>");
  append_page_footer();
  server.send(200, "text/html",webpage);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
File UploadFile; 
void handleFileUpload(){ // upload a new file to the Filing system
  HTTPUpload& uploadfile = server.upload(); // See https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WebServer/srcv
                                            // For further information on 'status' structure, there are other reasons such as a failed transfer that could be used
  if(uploadfile.status == UPLOAD_FILE_START)
  {
    String filename = uploadfile.filename;
    if(!filename.startsWith("/")) filename = "/"+filename;
    Serial.print("Upload File Name: "); Serial.println(filename);
    SPIFFS.remove(filename);                  // Remove a previous version, otherwise data is appended the file again
    UploadFile = SPIFFS.open(filename, "w");  // Open the file for writing in SPIFFS (create it, if doesn't exist)
  }
  else if (uploadfile.status == UPLOAD_FILE_WRITE)
  {
    if(UploadFile) UploadFile.write(uploadfile.buf, uploadfile.currentSize); // Write the received bytes to the file
  } 
  else if (uploadfile.status == UPLOAD_FILE_END)
  {
    if(UploadFile)          // If the file was successfully created
    {                                    
      UploadFile.close();   // Close the file again
      Serial.print("Upload Size: "); Serial.println(uploadfile.totalSize);
      webpage = "";
      append_page_header();
      webpage += F("<h3>File was successfully uploaded</h3>"); 
      webpage += F("<h2>Uploaded File Name: "); webpage += uploadfile.filename+"</h2>";
      webpage += F("<h2>File Size: "); webpage += file_size(uploadfile.totalSize) + "</h2><br>"; 
      append_page_footer();
      server.send(200,"text/html",webpage);
    } 
    else
    {
      ReportCouldNotCreateFile("upload");
    }
  }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void printDirectory(const char * dirname, uint8_t levels){
  File root = SPIFFS.open(dirname);
  if(!root){
    return;
  }
  if(!root.isDirectory()){
    return;
  }
  File file = root.openNextFile();
  while(file){
    if (webpage.length() > 1000) {
      SendHTML_Content();
    }
    if(file.isDirectory()){
      webpage += "<tr><td>"+String(file.isDirectory()?"Dir":"File")+"</td><td>"+String(file.name())+"</td><td></td></tr>";
      printDirectory(file.name(), levels-1);
    }
    else
    {
      webpage += "<tr><td>"+String(file.name())+"</td>";
      webpage += "<td>"+String(file.isDirectory()?"Dir":"File")+"</td>";
      int bytes = file.size();
      String fsize = "";
      if (bytes < 1024)                     fsize = String(bytes)+" B";
      else if(bytes < (1024 * 1024))        fsize = String(bytes/1024.0,3)+" KB";
      else if(bytes < (1024 * 1024 * 1024)) fsize = String(bytes/1024.0/1024.0,3)+" MB";
      else                                  fsize = String(bytes/1024.0/1024.0/1024.0,3)+" GB";
      webpage += "<td>"+fsize+"</td></tr>";
    }
    file = root.openNextFile();
  }
  file.close();
}

void Data(){
  SendHTML_Header();
  webpage += "<meta http-equiv='refresh' content='5'>";
  webpage += "<h3>System Information</h3>";
  webpage += "<h4>Bno080 Data</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Data</th><th>X</th><th>Y</th><th>Z</th><tr>";
  webpage += "<tr><td>Accel</td><td>"+ String(0.11) + "</td><td>" + String(0.01) + "</td><td>"+ String(0.99) +"</td></tr> ";
  webpage += "<tr><td>Gyro</td><td>"+ String(0.01) + "</td><td>" + String(0.01) + "</td><td>"+ String(0.01) +"</td></tr> ";
  webpage += "<tr><td>Magneto</td><td>"+ String(10) + "</td><td>" + String(12) + "</td><td>"+ String(90)+"</td></tr> ";
  webpage += "<tr><td>Euler</td><td>"+ String(3.1) + "</td><td>" + String(2) + "</td><td>"+  String(180)+"</td></tr> ";
  webpage += "</table>";
  webpage += "<h4>H3lis331dl Data</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Data</th><th>X</th><th>Y</th><th>Z</th><tr>";
  webpage += "<tr><td>Accel</td><td>"+ String(0.11) + "</td><td>" + String(0.01) + "</td><td>"+ String(0.99) +"</td></tr> ";
  webpage += "</table>";
  webpage += "<h4>MS5611</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Temp</th><th>Press</th><tr>";
  webpage += "<tr><td>"+String(21)+"</td><td>" + String(22) + "</td></tr>";
  webpage += "</table>";
  webpage += "<h4>Pyro Information</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Pyro 1</th><th>Pyro 2</th></tr>";
  webpage += "<tr><td>OK</td><td>OK</td></tr>";
  webpage += "</table> ";
  append_page_footer(); 
  SendHTML_Content();
  SendHTML_Stop();
}

void Settings(){
  SendHTML_Header();
  webpage += "<meta http-equiv='refresh' content='5'>";
  webpage += "<form action='/settings' method='POST'>";                                                                  
  webpage += "<h3>Settings</h3>";   
  webpage += "<label for='drouge'>Drouge to:</label>";
  webpage += "<input type='number' name='drouge'>";
  webpage += " ("+drouge+")<br>";
  webpage += "<label for='apooge'>Apooge to:</label>";
  webpage += "<input type='number' name='apooge'>";
  webpage += " ("+apooge+")<br>";      
  webpage += "Pyro ("+direction+") ";                                 
  webpage += "<input type='radio' name='direction' value='ON'>";  
  webpage += "<label for='ON'>ON</label>";
  webpage += "<input type='radio' name='direction' value='OFF'>";
  webpage += "<label for='ON'>OFF</label><br>";
  webpage += "<input type='submit' value='Set!'>";
  webpage += "</form>";  
  append_page_footer(); 
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void File_Delete(){
  if (server.args() > 0 ) { // Arguments were received
    if (server.hasArg("delete")) SPIFFS_file_delete(server.arg(0));
  }
  else SelectInput("Select a File to Delete","delete","delete");
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SPIFFS_file_delete(String filename) { // Delete the file 
  if (SPIFFS_present) { 
    SendHTML_Header();
    //File dataFile = SPIFFS.open("/"+filename, "r"); // Now read data from SPIFFS Card 
    //if (dataFile)
    //{
      if (SPIFFS.remove("/"+filename)) {
        Serial.println(F("File deleted successfully"));
        webpage += "<h3>File '"+filename+"' has been erased</h3>"; 
        webpage += F("<a href='/delete'>[Back]</a><br><br>");
      }
      else
      { 
        webpage += F("<h3>File was not deleted - error</h3>");
        webpage += F("<a href='delete'>[Back]</a><br><br>");
      }
    //} else ReportFileNotPresent("delete");
    append_page_footer(); 
    SendHTML_Content();
    SendHTML_Stop();
  } else ReportSPIFFSNotPresent();
} 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Header(){
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); 
  server.sendHeader("Pragma", "no-cache"); 
  server.sendHeader("Expires", "-1"); 
  server.setContentLength(CONTENT_LENGTH_UNKNOWN); 
  server.send(200, "text/html", ""); // Empty content inhibits Content-length header so we have to close the socket ourselves. 
  append_page_header();
  server.sendContent(webpage);
  webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Content(){
  server.sendContent(webpage);
  webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Stop(){
  server.sendContent("");
  server.client().stop(); // Stop is needed because no content length was sent
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SelectInput(String heading1, String command, String arg_calling_name){
  SendHTML_Header();
  webpage += F("<h3>"); webpage += heading1 + "</h3>"; 
  webpage += F("<FORM action='/"); webpage += command + "' method='post'>"; // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
  webpage += F("<input type='text' name='"); webpage += arg_calling_name; webpage += F("' value=''><br>");
  webpage += F("<type='submit' name='"); webpage += arg_calling_name; webpage += F("' value=''><br><br>");
  webpage += F("<a href='/'>[Back]</a><br><br>");
  webpage += F("<table align='center'>");
  webpage += F("<tr><th>Name/Type</th><th style='width:20%'>Type File/Dir</th><th>File Size</th></tr>");
  printDirectory("/",0);
  webpage += F("</table><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportSPIFFSNotPresent(){
  SendHTML_Header();
  webpage += F("<h3>No SPIFFS Card present</h3>"); 
  webpage += F("<a href='/'>[Back]</a><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportFileNotPresent(String target){
  SendHTML_Header();
  webpage += F("<h3>File does not exist</h3>"); 
  webpage += F("<a href='/"); webpage += target + "'>[Back]</a><br><br>";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportCouldNotCreateFile(String target){
  SendHTML_Header();
  webpage += F("<h3>Could Not Create Uploaded File (write-protected?)</h3>"); 
  webpage += F("<a href='/"); webpage += target + "'>[Back]</a><br><br>";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
String file_size(int bytes){
  String fsize = "";
  if (bytes < 1024)                 fsize = String(bytes)+" B";
  else if(bytes < (1024*1024))      fsize = String(bytes/1024.0,3)+" KB";
  else if(bytes < (1024*1024*1024)) fsize = String(bytes/1024.0/1024.0,3)+" MB";
  else                              fsize = String(bytes/1024.0/1024.0/1024.0,3)+" GB";
  return fsize;
}