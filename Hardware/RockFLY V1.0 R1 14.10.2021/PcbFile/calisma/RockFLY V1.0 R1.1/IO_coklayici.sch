EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 14 15
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5850 3450 0    50   Input ~ 0
SDA
Text HLabel 5850 3350 0    50   Input ~ 0
SCL
$Comp
L power:+3.3V #PWR?
U 1 1 61266D57
P 6450 2950
AR Path="/6115C67C/61266D57" Ref="#PWR?"  Part="1" 
AR Path="/611727C1/61266D57" Ref="#PWR?"  Part="1" 
AR Path="/61178059/61266D57" Ref="#PWR?"  Part="1" 
AR Path="/6111A12F/61266D57" Ref="#PWR?"  Part="1" 
AR Path="/616DCCBE/61266D57" Ref="#PWR073"  Part="1" 
F 0 "#PWR073" H 6450 2800 50  0001 C CNN
F 1 "+3.3V" H 6465 3123 50  0000 C CNN
F 2 "" H 6450 2950 50  0001 C CNN
F 3 "" H 6450 2950 50  0001 C CNN
	1    6450 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3450 5850 3450
Wire Wire Line
	5850 3350 5950 3350
$Comp
L power:GND #PWR?
U 1 1 61264B89
P 5850 3900
AR Path="/6111A211/61264B89" Ref="#PWR?"  Part="1" 
AR Path="/6111A19E/61264B89" Ref="#PWR?"  Part="1" 
AR Path="/6111A12F/61264B89" Ref="#PWR?"  Part="1" 
AR Path="/616DCCBE/61264B89" Ref="#PWR074"  Part="1" 
F 0 "#PWR074" H 5850 3650 50  0001 C CNN
F 1 "GND" H 5855 3727 50  0000 C CNN
F 2 "" H 5850 3900 50  0001 C CNN
F 3 "" H 5850 3900 50  0001 C CNN
	1    5850 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61267E80
P 6450 4550
AR Path="/6111A211/61267E80" Ref="#PWR?"  Part="1" 
AR Path="/6111A19E/61267E80" Ref="#PWR?"  Part="1" 
AR Path="/6111A12F/61267E80" Ref="#PWR?"  Part="1" 
AR Path="/616DCCBE/61267E80" Ref="#PWR075"  Part="1" 
F 0 "#PWR075" H 6450 4300 50  0001 C CNN
F 1 "GND" H 6455 4377 50  0000 C CNN
F 2 "" H 6450 4550 50  0001 C CNN
F 3 "" H 6450 4550 50  0001 C CNN
	1    6450 4550
	1    0    0    -1  
$EndComp
$Comp
L Interface_Expansion:PCF8574 U10
U 1 1 616DDAD0
P 6450 3750
F 0 "U10" H 6100 4400 50  0000 C CNN
F 1 "PCF8574" H 6200 3100 50  0000 C CNN
F 2 "digikey-footprints:VFQFN-16-1EP_3x3mm" H 6450 3750 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/PCF8574_PCF8574A.pdf" H 6450 3750 50  0001 C CNN
	1    6450 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3650 5850 3750
Wire Wire Line
	5850 3650 5950 3650
Wire Wire Line
	5950 3750 5850 3750
Connection ~ 5850 3750
Wire Wire Line
	5850 3750 5850 3850
Wire Wire Line
	5950 3850 5850 3850
Connection ~ 5850 3850
Wire Wire Line
	5850 3850 5850 3900
Wire Wire Line
	6450 2950 6450 3050
Wire Wire Line
	6450 4450 6450 4550
Text HLabel 7050 3350 2    50   Input ~ 0
Led1
Text HLabel 7050 3450 2    50   Input ~ 0
Led2
Text HLabel 7050 3550 2    50   Input ~ 0
Pyro1
Text HLabel 7050 3650 2    50   Input ~ 0
Pyro2
Wire Wire Line
	7050 3350 6950 3350
Wire Wire Line
	7050 3450 6950 3450
Wire Wire Line
	7050 3550 6950 3550
Wire Wire Line
	7050 3650 6950 3650
$EndSCHEMATC
