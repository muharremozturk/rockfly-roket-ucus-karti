#include <WiFi.h>
#include <ESP32Ping.h>
#include <Wire.h>                                  //Needed for I2C to GNSS
#include <SparkFun_u-blox_GNSS_Arduino_Library.h>  //http://librarymanager/All#SparkFun_u-blox_GNSS
SFE_UBLOX_GNSS myGNSS;

long lastTime = 0;  //Simple local timer. Limits amount if I2C traffic to u-blox module.

const char* ssid = "VODAFONE_70CD";
const char* password = "ulaseren9519";

TaskHandle_t Task1;
void setup() {
  Serial.begin(115200);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi...");
  }
  Wire.begin(26,27);

  if (myGNSS.begin() == false)  //Connect to the u-blox module using Wire port
  {
    Serial.println(F("u-blox GNSS not detected at default I2C address. Please check wiring. Freezing."));
    while (1)
      ;
  }
  myGNSS.setUART1Output(0);            //Turn off UBX and RTCM sentences on the UART1 interface
  myGNSS.setI2COutput(COM_TYPE_UBX);  //Set the I2C port to output UBX only (turn off NMEA noise)
  myGNSS.setNavigationFrequency(1);
  xTaskCreatePinnedToCore(
    Task1code, /* Task function. */
    "Task1",   /* name of task. */
    10000,     /* Stack size of task */
    NULL,      /* parameter of the task */
    1,         /* priority of the task */
    &Task1,    /* Task handle to keep track of created task */
    0);        /* pin task to core 0 */
}

void Task1code(void* pvParameters) {
  Serial.print("Task1 running on core ");
  Serial.println(xPortGetCoreID());


  for (;;) {
    vTaskDelay(1);
    bool success = Ping.ping("www.google.com", 3);

    if (!success) {
      Serial.println("Ping failed");
      return;
    }

    Serial.println("Ping succesful.");
    //delay(100);
  }
}
void loop() {

  if (millis() - lastTime > 1000) {
    lastTime = millis();  //Update the timer

    long latitude = myGNSS.getLatitude();
    Serial.print(F("Lat: "));
    Serial.print(latitude);

    long longitude = myGNSS.getLongitude();
    Serial.print(F(" Long: "));
    Serial.print(longitude);
    Serial.print(F(" (degrees * 10^-7)"));

    long altitude = myGNSS.getAltitude();
    Serial.print(F(" Alt: "));
    Serial.print(altitude);
    Serial.print(F(" (mm)"));

    byte SIV = myGNSS.getSIV();
    Serial.print(F(" SIV: "));
    Serial.print(SIV);

    Serial.println();
    Serial.print(myGNSS.getYear());
    Serial.print("-");
    Serial.print(myGNSS.getMonth());
    Serial.print("-");
    Serial.print(myGNSS.getDay());
    Serial.print(" ");
    Serial.print(myGNSS.getHour());
    Serial.print(":");
    Serial.print(myGNSS.getMinute());
    Serial.print(":");
    Serial.print(myGNSS.getSecond());

    Serial.print("  Time is ");
    if (myGNSS.getTimeValid() == false) {
      Serial.print("not ");
    }
    Serial.print("valid  Date is ");
    if (myGNSS.getDateValid() == false) {
      Serial.print("not ");
    }
    Serial.print("valid");

    Serial.println();
  }
}