/*
   RadioLib SX126x Ping-Pong Example

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>
#include "SPI.h"
SPIClass SPI2(HSPI);
// uncomment the following only on one
// of the nodes to initiate the pings


// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33, SPI2);

float H3LIS331_data[3] = { 10, 20, 30 };
float Ms5611_data[2] = { 10, 20 };
float bnoAccel[3] = { 10, 20, 30 };
float bnoGyro[3] = { 10, 20, 30 };
float bnoMag[3] = { 10, 20, 30 };
float bnoRaw[3] = { 10, 20, 30 };
long GPS_data[3] = { 10, 20, 30 };
int GPS_time[6] = { 10, 20, 30, 10, 20, 30 };
uint8_t SIV = 3;

struct Data {
  byte byteArray[100];
  int counter = 0;
};
Data data;
// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;

int apogee;
int droug;
float carrierFreq = 915;
uint8_t outputPower = 20;
bool flightMode = 0;

byte passw = 0x60;
byte errorData = 0x00;
byte setConfig = 0x10;
byte sensData = 0x20;
byte getConfigData = 0x30;
byte sendConfig = 0x40;

bool error=0;
bool sendSensorData=0;
bool sendConfigData=0;
bool setConOk=0;
// save transmission states between loops
int transmissionState = ERR_NONE;

// flag to indicate transmission or reception state
bool transmitFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// flag to indicate that a packet was sent or received
volatile bool operationDone = false;

// this function is called when a complete packet
// is transmitted or received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if (!enableInterrupt) {
    return;
  }

  // we sent aor received  packet, set the flag
  operationDone = true;
}

void setup() {
  Serial.begin(115200);
  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;
  const int HSPI_CS = 15;

  SPI2.begin(HSPI_SCK, HSPI_MISO, HSPI_MOSI, HSPI_CS);
  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  // carrier frequency:           915.0 MHz
  // bandwidth:                   500.0 kHz
  // spreading factor:            6
  // coding rate:                 5
  // sync word:                   0x34 (public network/LoRaWAN)
  // output power:                2 dBm
  // preamble length:             20 symbols
  int state = radio.begin(carrierFreq, 500.0, 6, 5, 0x34, outputPower);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }

  // set the function that will be called
  // when new packet is received
  radio.setDio1Action(setFlag);

  // start listening for LoRa packets on this node
  Serial.print(F("[SX1262] Starting to listen ... "));
  state = radio.startReceive();
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }

}

void loop() {
  if (!flightMode) {
    // check if the previous operation finished
    if (operationDone) {
      // disable the interrupt service routine while
      // processing the data
      enableInterrupt = false;

      // reset flag
      operationDone = false;

      if (transmitFlag) {
        // the previous operation was transmission, listen for response
        // print the result
        if (transmissionState == ERR_NONE) {
          // packet was successfully sent
          Serial.println(F("transmission finished!"));

        } else {
          Serial.print(F("failed, code "));
          Serial.println(transmissionState);
        }
        radio.startReceive();

        transmitFlag = false;

      } else {
        // the previous operation was reception
        // print data and send another packet
        int state = radio.readData(data.byteArray, 15);

        if (state == ERR_NONE) {
          // packet was successfully received

          Serial.println(F("[SX1262] Received packet!"));
          if (data.byteArray[0] == passw && data.byteArray[1] == getConfigData) {
            error=0;
            sendConfigData=1;
            data.counter=data.counter+2;              
            byteToInt(&apogee, &data.counter, data.byteArray);
            Serial.println(apogee);
            byteToInt(&droug, &data.counter, data.byteArray);
            Serial.println(droug);
            byteToFloat(&carrierFreq, &data.counter, data.byteArray);
            Serial.println(carrierFreq);
            byteToInt8(&outputPower, &data.counter, data.byteArray);
            Serial.println(outputPower);
            
          }else if(data.byteArray[0] == passw && data.byteArray[1] == sendConfig){
			  
            error=0;
            sendConfigData=1;
            
          }else if(data.byteArray[0] == passw && data.byteArray[1] == setConfig){
            
            error=0;
            setConOk=1;
            radio.setFrequency(carrierFreq);
            radio.setOutputPower(outputPower);
            
          }else if(data.byteArray[0] == passw && data.byteArray[1] == sensData){
            sendSensorData=1;
          }
          else{
            error=1;
          }
        }
        // wait a second before transmitting again
        delay(1000);

        // send another one
        Serial.print(F("[SX1262] Sending another packet ... "));
        if(error==0&&sendConfigData==1){
          sendConfigData=0;
          //güncell ayarlar
          data.counter=0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sendConfig, &data.counter, data.byteArray);
          longAddByte(apogee, &data.counter, data.byteArray);
          longAddByte(droug, &data.counter, data.byteArray);
          floatAddByte(carrierFreq, &data.counter, data.byteArray);
          byteAddByte(outputPower, &data.counter, data.byteArray); 
          transmissionState = radio.startTransmit(data.byteArray,15);
        }else if(error==0&&setConOk==1){  
          setConOk=0;  
          data.counter=0; 
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(setConfig, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray,2);      
        }else if(error==0&&sendSensorData==1){
          sendSensorData=0;
          data.counter=0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sensData, &data.counter, data.byteArray);
          for (int k = 0; k < 3; k++) {
            floatAddByte(H3LIS331_data[k], &data.counter, data.byteArray);
          }

          for (int k = 0; k < 2; k++) {
            floatAddByte(Ms5611_data[k], &data.counter, data.byteArray);
          }

          for (int k = 0; k < 3; k++) {
            floatAddByte(bnoAccel[k], &data.counter, data.byteArray);
          }
          for (int k = 0; k < 3; k++) {
            floatAddByte(bnoGyro[k], &data.counter, data.byteArray);
          }
          for (int k = 0; k < 3; k++) {
            floatAddByte(bnoMag[k], &data.counter, data.byteArray);
          }
          for (int k = 0; k < 3; k++) {
            floatAddByte(bnoRaw[k], &data.counter, data.byteArray);
          } 

          for (int k = 0; k < 3; k++) {
            longAddByte(GPS_data[k], &data.counter, data.byteArray);
          }
          byteAddByte(SIV, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray,90);
        }else{
          error=0;
          data.counter=0;
          byteAddByte(passw, &data.counter, data.byteArray);
          //hatali mesaj
          byteAddByte(errorData, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray,15);
        }
        
        transmitFlag = true;
      }

      // we're ready to process more packets,
      // enable interrupt service routine
      enableInterrupt = true;

      //clearDio1Action();
      //flightMode=1;
    }
  } else {
    //ucus kodları
  }
}

void byteToFloat(float *data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&data)[i] = array[*count];
    (*count)++;
  }
}

void byteToInt(int *data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&data)[i] = array[*count];
    (*count)++;
  }
}

void byteToInt16(uint16_t *data, int *count, byte *array) {
  for (int i = 0; i < 2; i++) {
    ((uint8_t *)&data)[i] = array[*count];
    (*count)++;
  }
}

void byteToInt8(uint8_t *data, int *count, byte *array) {
  ((uint8_t *)&data)[0] = array[*count];
  (*count)++;
}

void floatAddByte(float data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}

void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}