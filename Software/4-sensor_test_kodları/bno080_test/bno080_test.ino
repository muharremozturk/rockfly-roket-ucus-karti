#include <Wire.h>

#include "SparkFun_BNO080_Arduino_Library.h" // Click here to get the library: http://librarymanager/All#SparkFun_BNO080
BNO080 myIMU;

float x_Accel,y_Accel,z_Accel;
float x_Gyro,y_Gyro,z_Gyro;
float x_Mag,y_Mag,z_Mag;
float Roll,Pitch,Yaw;

void setup()
{
  Serial.begin(115200);
  
  Wire.begin();

  myIMU.begin();

  Wire.setClock(400000); //Increase I2C data rate to 400kHz

  myIMU.enableAccelerometer(50); //Send data update every 50ms
  myIMU.enableGyro(50); //Send data update every 50ms
  myIMU.enableMagnetometer(50); //Send data update every 50ms
  myIMU.enableRotationVector(50);

  Serial.println(F("Accelerometer enabled"));
  Serial.println(F("Output in form x, y, z, in m/s^2"));
  Serial.println(F("Gyro enabled"));
  Serial.println(F("Output in form x, y, z, in radians per second"));
  Serial.println(F("Magnetometer enabled"));
  Serial.println(F("Output in form x, y, z, in uTesla"));
  Serial.println(F("Rotation vector enabled"));
  Serial.println(F("Output in form roll, pitch, yaw"));
}
unsigned long start=0;
unsigned long end=0;
void loop()
{
  //Look for reports from the IMU
  if (myIMU.dataAvailable() == true)
  {
    start=micros();
    x_Accel = myIMU.getAccelX();
    y_Accel = myIMU.getAccelY();
    z_Accel = myIMU.getAccelZ();
    end=micros();
    Serial.printf(" suresi  %d Accel :",end-start);
    Serial.print(x_Accel, 2);
    Serial.print(F(","));
    Serial.print(y_Accel, 2);
    Serial.print(F(","));
    Serial.println(z_Accel, 2);
    
    start=micros();
    x_Gyro = myIMU.getGyroX();
    y_Gyro = myIMU.getGyroY();
    z_Gyro = myIMU.getGyroZ();
    end=micros();
    Serial.printf(" suresi  %d Gyro: ",end-start);
    Serial.print(x_Gyro, 2);
    Serial.print(F(","));
    Serial.print(y_Gyro, 2);
    Serial.print(F(","));
    Serial.println(z_Gyro, 2);
    
    start=micros();
    x_Mag = myIMU.getMagX();
    y_Mag = myIMU.getMagY();
    z_Mag = myIMU.getMagZ();
    end=micros();
    Serial.printf(" suresi :%d Mag:",end-start);
    Serial.print(x_Mag, 2);
    Serial.print(F(","));
    Serial.print(y_Mag, 2);
    Serial.print(F(","));
    Serial.println(z_Mag, 2);
    
    start=micros();
    Roll =(myIMU.getRoll()) * 180.0 / PI;
    Pitch = (myIMU.getPitch()) * 180.0 / PI;
    Yaw = (myIMU.getYaw()) * 180.0 / PI;
    end=micros();
    Serial.printf(" suresi: %d Pitch:",end-start);
    Serial.print(Roll, 1);
    Serial.print(F(","));
    Serial.print(Pitch, 1);
    Serial.print(F(","));
    Serial.println(Yaw, 1);
  }
}
