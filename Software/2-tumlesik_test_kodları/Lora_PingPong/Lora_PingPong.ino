/*
   RadioLib SX126x Ping-Pong Example

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>
#include "SPI.h"
SPIClass SPI2(HSPI);
// uncomment the following only on one
// of the nodes to initiate the pings
#define INITIATING_NODE

// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33,SPI2);

struct Data{
byte byteArray[100];
int counter=0;
};

// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;
long apogee=10000;
long droug=5000;
long carrierFreq=915;
int8_t outputPower=20;
int8_t loraReSetup=0;
// save transmission states between loops
int transmissionState = ERR_NONE;

// flag to indicate transmission or reception state
bool transmitFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// flag to indicate that a packet was sent or received
volatile bool operationDone = false;

// this function is called when a complete packet
// is transmitted or received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if(!enableInterrupt) {
    return;
  }

  // we sent aor received  packet, set the flag
  operationDone = true;
}
int i=0;
void setup() {
  Serial.begin(115200);
  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;  
  const int HSPI_CS = 15;

  SPI2.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS); 
  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  // carrier frequency:           915.0 MHz
  // bandwidth:                   500.0 kHz
  // spreading factor:            6
  // coding rate:                 5
  // sync word:                   0x34 (public network/LoRaWAN)
  // output power:                2 dBm
  // preamble length:             20 symbols
  int state = radio.begin(carrierFreq, 500.0, 6, 5, 0x34, outputPower);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }

  // set the function that will be called
  // when new packet is received
  radio.setDio1Action(setFlag);

  #if defined(INITIATING_NODE)
    // send the first packet on this node
    Serial.print(F("[SX1262] Sending first packet ... "));
    transmissionState = radio.startTransmit("Hello World!");
    transmitFlag = true;
  #else
    // start listening for LoRa packets on this node
    Serial.print(F("[SX1262] Starting to listen ... "));
    state = radio.startReceive();
    if (state == ERR_NONE) {
      Serial.println(F("success!"));
    } else {
      Serial.print(F("failed, code "));
      Serial.println(state);
      while (true);
    }
  #endif
}

void loop() {
  // check if the previous operation finished
  if(operationDone) {
    // disable the interrupt service routine while
    // processing the data
    enableInterrupt = false;

    // reset flag
    operationDone = false;
    
    if(transmitFlag) {
      // the previous operation was transmission, listen for response
      // print the result
      if (transmissionState == ERR_NONE) {
        // packet was successfully sent
        Serial.println(F("transmission finished!"));
        if(loraReSetup==127){
          loraReSetup=0;
          radio.setFrequency(carrierFreq);
          radio.setOutputPower(outputPower);
          //delay(100);
        }  
      } else {
        Serial.print(F("failed, code "));
        Serial.println(transmissionState);
  
      }

      // listen for response
      radio.startReceive();
      transmitFlag = false;
      
    } else {
      // the previous operation was reception
      // print data and send another packet
      String str;
      
      int state = radio.readData(str);
      
      if (state == ERR_NONE) {
        // packet was successfully received
        Serial.println(F("[SX1262] Received packet!"));
      
        // print data of the packet
        Serial.print(F("[SX1262] Data:\t\t"));
        Serial.println(str);
  
        // print RSSI (Received Signal Strength Indicator)
        Serial.print(F("[SX1262] RSSI:\t\t"));
        Serial.print(radio.getRSSI());
        Serial.println(F(" dBm"));
  
        // print SNR (Signal-to-Noise Ratio)
        Serial.print(F("[SX1262] SNR:\t\t"));
        Serial.print(radio.getSNR());
        Serial.println(F(" dB"));
  
      }
      
      // wait a second before transmitting again
      delay(1000);
      if(i==3){
        loraReSetup=127;
        carrierFreq=868;
        outputPower=15;
        apogee=8100;
      }
      i++;
      // send another one
      Serial.print(F("[SX1262] Sending another packet ... "));
      Data data;
      longAddByte(apogee,&data.counter,data.byteArray);
      longAddByte(droug,&data.counter,data.byteArray);
      longAddByte(carrierFreq,&data.counter,data.byteArray);   
      int8_tAddByte(outputPower,&data.counter,data.byteArray); 
      int8_tAddByte(loraReSetup,&data.counter,data.byteArray);         
      transmissionState = radio.startTransmit(data.byteArray,14);
      transmitFlag = true;
      
      
    }

    // we're ready to process more packets,
    // enable interrupt service routine
    enableInterrupt = true;
    
  }
}

void floatAddByte(float data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void int8_tAddByte(int8_t data,int *count,byte *array){
    array[*count]=((uint8_t*)&data)[0];
    (*count)++;
}

void byteAddByte(byte data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}