#include <Wire.h>
#include <Adafruit_H3LIS331.h>
#include <Adafruit_Sensor.h>
#include "MS5611.h"
#include "SparkFun_BNO080_Arduino_Library.h"
#include <SparkFun_u-blox_GNSS_Arduino_Library.h>  //http://librarymanager/All#SparkFun_u-blox_GNSS
#include "FS.h"
#include "LITTLEFS.h"
#include "ESP32TimerInterrupt.h"
#include "CC1200.h"
#include "TimeLib.h"


#define SPIFFS LITTLEFS
#define FORMAT_LITTLEFS_IF_FAILED true

#define TIMER_INTERRUPT_DEBUG 1
#define _TIMERINTERRUPT_LOGLEVEL_ 4

#define THIS_NODE 0x01
#define TARG_NODE 0x02
#define RadioTXRXpin 16  // CC1200 Packet Semaphore
// BUFFER
#define FIFO_SIZE 100      // TX and RX Buffer Size
byte readBytes;            // Number of bytes read from RX FIFO
byte rxBuffer[FIFO_SIZE];  // Fill with RX FIFO
byte writeBytes;           // Number of bytes written to TX FIFO
byte txBuffer[FIFO_SIZE];  // Fill with TX FIFO

#define idxLength 0      // Length index
#define idxTargetNode 1  // TargetNode index
#define idxFrameCount 3  // FrameCount index
#define TIMEOUT 100      // TIMEOUT
union DoubleBytes {
  double DoubleVal;
  byte AsBytes[sizeof(double)];  // 4 Bytes
} DoubleBytes;
byte counterTx = 0x00;
volatile bool packetSemaphore;

struct Data {
  byte byteArray[100];
  int counter = 0;
};
Data data;

File file;
//sensor
Adafruit_H3LIS331 lis = Adafruit_H3LIS331();
SFE_UBLOX_GNSS myGNSS;
MS5611 MS5611(0x76);  // 0x76 = CSB to VCC; 0x77 = CSB to GND
sensors_event_t event;
BNO080 myIMU;
//Dual Core
TaskHandle_t Task1;
//sensor data
float H3LIS331_data[3];
float Ms5611_data[2];
float bnoAccel[3];
float bnoGyro[3];
float bnoMag[3];
float bnoRaw[3];
long GPS_data[3];
int GPS_time[6];
tmElements_t my_time;          // time elements structure
unsigned long unix_timestamp;  // a timestamp
byte SIV;
String All_data1[10];
String All_data2[10];
String All_data_flash1[10];
String All_data_flash2[10];
String header = "400gAcceloX,400gAcceloY,400gAcceloZ,MS5611Temp,MS5611Pressure,BNOAccelX,BNOAccelY,BNOAccelZ,BNOGyroX,BNOGyroY,BNOGyroZ,BNOMagX,BNOMagY,BNOMagZ,Roll,Pitch,Yaw,GpsLatitude,GpsLongitude,GpsAltitude,GpsDate,GpsTime\n";
int hafiza = 0;
int i = 0;
bool count = 0;
uint8_t countGps = 0;
uint8_t countCC1200 = 0;
//hafıza
bool flashCheck1 = 0;
bool flashCheck2 = 0;
bool writeCheck1 = 0;
bool writeCheck2 = 0;
bool dataCheck = 0;
bool cc1200Check = 0;
bool writeStart = 0;

//timer
ESP32Timer ITimer1(1);

//fonksiyon prototipleri
void sensorBegin();
void readSensor();
void flashRead();
void flashClean();
void flashCreatPart();
void flashWrite();
void sensorStart();
void readH3LIS331();
void readBno080();

void readMS5611();
void readGPS();
void writeData();
void IRAM_ATTR TimerHandler1(void);
void listDir(fs::FS &fs, const char *dirname, uint8_t levels);
void createDir(fs::FS &fs, const char *path);
void removeDir(fs::FS &fs, const char *path);
void readFile(fs::FS &fs, const char *path);
void writeFile(fs::FS &fs, const char *path, String message);
void appendFile(fs::FS &fs, const char *path, String message);
void deleteFile(fs::FS &fs, const char *path);
void floatAddByte(float data, int *count, byte *array);
void longAddByte(long data, int *count, byte *array);
void byteAddByte(byte data, int *count, byte *array);

void setup() {

  Serial.begin(115200);
  //buzzer();
  Wire.begin();
  sensorBegin();
  Wire.setClock(400000);
  //hafıza baslat
  SPIFFS.begin(1);
  flashRead();
  flashClean();
  //baslık dosyasını yaz
  writeFile(SPIFFS, "/Data.txt", header);
  flashCreatPart();
  //listDir(SPIFFS, "/", 0);
  //timer ayar
  //cc1200Begin();
  buzzer();
  timerBegin();

  //create a task that will be executed in the Task1code() function, with priority 1 and executed on core 0
  xTaskCreatePinnedToCore(
    Task1code, /* Task function. */
    "Task1",   /* name of task. */
    10000,     /* Stack size of task */
    NULL,      /* parameter of the task */
    1,         /* priority of the task */
    &Task1,    /* Task handle to keep track of created task */
    0);        /* pin task to core 0 */
}

unsigned long start = 0;
unsigned long end = 0;

//Data read
void Task1code(void *pvParameters) {
  Serial.print("Task1 running on core ");
  Serial.println(xPortGetCoreID());


  for (;;) {
    vTaskDelay(1);
    flashWrite();
    //cc1200Transmit();
  }
}

void loop() {
  vTaskDelay(1);

  if (count == 1) {
    // Comment out enter / exit to deactivate the critical section
    count = 0;
    //start = millis();
    readSensor();
    //end = millis();
    //Serial.printf("okuma suresi: %d \n", end - start);
  }
}

void sensorBegin() {

  myIMU.begin();
  myIMU.enableAccelerometer(20);  //Send data update every 20ms
  myIMU.enableGyro(20);           //Send data update every 20ms
  myIMU.enableMagnetometer(20);   //Send data update every 20ms
  myIMU.enableRotationVector(20);

  MS5611.begin();

  lis.begin_I2C();                     // change this to 0x19 for alternative i2c address
  lis.setRange(H3LIS331_RANGE_100_G);  // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_50_HZ);
  /*
  //GPS ayar
  myGNSS.begin();
  myGNSS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)
  myGNSS.saveConfigSelective(VAL_CFG_SUBSEC_IOPORT); //Save (only) the communications port settings to flash and BBR
  myGNSS.setNavigationFrequency(10); //Produce two solutions per second
  myGNSS.setAutoPVTcallback(&printPVTdata); // Enable automatic NAV PVT messages with callback to printPVTdata
  //myGNSS.saveConfiguration();        //Save the current settings to flash and BBR
  */
}

//sensor okuma fonksiyonu
void readSensor() {
  start = millis();
  sensorStart();
  readBno080();
  vTaskDelay(1);
  readH3LIS331();
  readMS5611();
  end = millis();
  Serial.printf("okuma suresi: %d \n", end - start);
  //readGPS();
  toByte();
  writeData();
}
unsigned long start2 = 0;
unsigned long end2 = 0;
void sensorStart() {
  MS5611.read();
  lis.getEvent(&event);
  myIMU.dataAvailable();
}

void readH3LIS331() {
  H3LIS331_data[0] = event.acceleration.x / SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[1] = event.acceleration.y / SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[2] = event.acceleration.z / SENSORS_GRAVITY_STANDARD;
}

void readBno080() {
  start2 = millis();
  bnoAccel[0] = myIMU.getAccelX() / SENSORS_GRAVITY_STANDARD;
  bnoAccel[1] = myIMU.getAccelY() / SENSORS_GRAVITY_STANDARD;
  bnoAccel[2] = myIMU.getAccelZ() / SENSORS_GRAVITY_STANDARD;
  bnoGyro[0] = myIMU.getGyroX();
  bnoGyro[1] = myIMU.getGyroY();
  bnoGyro[2] = myIMU.getGyroZ();
  bnoMag[0] = myIMU.getMagX();
  bnoMag[1] = myIMU.getMagY();
  bnoMag[2] = myIMU.getMagZ();
  bnoRaw[0] = (myIMU.getRoll()) * 180.0 / PI;
  bnoRaw[1] = (myIMU.getPitch()) * 180.0 / PI;
  bnoRaw[2] = (myIMU.getYaw()) * 180.0 / PI;
}

void readMS5611() {
  Ms5611_data[0] = MS5611.getTemperature();
  Ms5611_data[1] = MS5611.getPressure() * 0.010197;
}
void printPVTdata(UBX_NAV_PVT_data_t ubxDataStruct) {
  GPS_data[0] = ubxDataStruct.lat;   // Print the latitude
  GPS_data[1] = ubxDataStruct.lon;   // Print the longitude
  GPS_data[2] = ubxDataStruct.hMSL;  // Print the height above mean sea level
  
  GPS_time[0] = ubxDataStruct.year;
  GPS_time[1] = ubxDataStruct.month;
  GPS_time[2] = ubxDataStruct.day;
  GPS_time[3] = ubxDataStruct.hour;
  GPS_time[4] = ubxDataStruct.min;
  GPS_time[5] = ubxDataStruct.sec;
  my_time.Year = GPS_time[0] - 1970;
  my_time.Month = GPS_time[1];
  my_time.Day = GPS_time[2];
  my_time.Hour = GPS_time[3];
  my_time.Minute = GPS_time[4];
  my_time.Second = GPS_time[5];
  unix_timestamp = (long)(makeTime(my_time));
}
void readGPS() {

  if (countGps >= 4) {
    myGNSS.checkUblox();      // Check for the arrival of new data and process it.
    myGNSS.checkCallbacks();  // Check if any callbacks are waiting to be processed.
  }
}

void toByte() {
  if ((countCC1200 >= 4) && (cc1200Check == 0)) {
    countCC1200 = 0;
    for (int k = 0; k < 3; k++) {
      floatAddByte(H3LIS331_data[k], &data.counter, data.byteArray);
    }

    for (int k = 0; k < 2; k++) {
      floatAddByte(Ms5611_data[k], &data.counter, data.byteArray);
    }

    for (int k = 0; k < 3; k++) {
      floatAddByte(bnoAccel[k], &data.counter, data.byteArray);
    }
    for (int k = 0; k < 3; k++) {
      floatAddByte(bnoGyro[k], &data.counter, data.byteArray);
    }
    for (int k = 0; k < 3; k++) {
      floatAddByte(bnoMag[k], &data.counter, data.byteArray);
    }
    for (int k = 0; k < 3; k++) {
      floatAddByte(bnoRaw[k], &data.counter, data.byteArray);
    }

    for (int k = 0; k < 3; k++) {
      longAddByte(GPS_data[k], &data.counter, data.byteArray);
    }
    byteAddByte(SIV, &data.counter, data.byteArray);
    longAddByte(unix_timestamp, &data.counter, data.byteArray);
    data.counter = 0;
    cc1200Check = 1;
  }
}

void writeData() {

  while (flashCheck1 && flashCheck2) {
    vTaskDelay(1);
  }

  if (dataCheck == 0) {
    All_data1[i] = String(H3LIS331_data[0]) + "," + String(H3LIS331_data[1]) + "," + String(H3LIS331_data[2]) + ",";
    All_data1[i] += String(Ms5611_data[0]) + "," + String(Ms5611_data[1]) + ",";
    All_data1[i] += String(bnoAccel[0]) + "," + String(bnoAccel[1]) + "," + String(bnoAccel[2]) + ",";
    All_data1[i] += String(bnoGyro[0]) + "," + String(bnoGyro[1]) + "," + String(bnoGyro[2]) + ",";
    All_data1[i] += String(bnoMag[0]) + "," + String(bnoMag[1]) + "," + String(bnoMag[2]) + ",";
    All_data1[i] += String(bnoRaw[0]) + "," + String(bnoRaw[1]) + "," + String(bnoRaw[2]) + ",";
    All_data1[i] += String(GPS_data[0]) + "," + String(GPS_data[1]) + "," + String(GPS_data[2]) + ",";
    All_data1[i] += String(SIV) + ",";
    All_data1[i] += String(GPS_time[0]) + "-" + String(GPS_time[1]) + "-" + String(GPS_time[2]) + ",";
    All_data1[i] += String(GPS_time[3]) + "." + String(GPS_time[4]) + "." + String(GPS_time[5]) + "\n";

    if (i == 9) {
      i = 0;
      flashCheck1 = 1;
      dataCheck = 1;
    } else {
      i++;
    }
  } else if (dataCheck == 1) {
    All_data2[i] = String(H3LIS331_data[0]) + "," + String(H3LIS331_data[1]) + "," + String(H3LIS331_data[2]) + ",";
    All_data2[i] += String(Ms5611_data[0]) + "," + String(Ms5611_data[1]) + ",";
    All_data2[i] += String(bnoAccel[0]) + "," + String(bnoAccel[1]) + "," + String(bnoAccel[2]) + ",";
    All_data2[i] += String(bnoGyro[0]) + "," + String(bnoGyro[1]) + "," + String(bnoGyro[2]) + ",";
    All_data2[i] += String(bnoMag[0]) + "," + String(bnoMag[1]) + "," + String(bnoMag[2]) + ",";
    All_data2[i] += String(bnoRaw[0]) + "," + String(bnoRaw[1]) + "," + String(bnoRaw[2]) + ",";
    All_data2[i] += String(GPS_data[0]) + "," + String(GPS_data[1]) + "," + String(GPS_data[2]) + ",";
    All_data2[i] += String(SIV) + ",";
    All_data2[i] += String(GPS_time[0]) + "-" + String(GPS_time[1]) + "-" + String(GPS_time[2]) + ",";
    All_data2[i] += String(GPS_time[3]) + "." + String(GPS_time[4]) + "." + String(GPS_time[5]) + "\n";

    if (i == 9) {
      i = 0;
      flashCheck2 = 1;
      dataCheck = 0;
    } else {
      i++;
    }
  }
}

void flashWrite() {

  if (flashCheck1 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash1[j] = All_data1[j];
    }
    flashCheck1 = 0;
    writeCheck1 = 1;
  }

  if (flashCheck2 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash2[j] = All_data2[j];
    }
    flashCheck2 = 0;
    writeCheck2 = 1;
  }

  if (writeCheck1 == 1) {
    flashWriteData(All_data_flash1);
    writeCheck1 = 0;
  }

  if (writeCheck2 == 1) {
    flashWriteData(All_data_flash2);
    writeCheck2 = 0;
  }
}

void flashWriteData(String a[10]) {
  if (hafiza == 0) {
    file = SPIFFS.open("/Data.txt", FILE_APPEND);
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else if (hafiza < 15) {
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else {
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    file.close();
    hafiza = 0;
  }
}
void cc1200Begin() {
  cc1200.Init(SS, MOSI, MISO, SCK, PIN_UNUSED);       // SS, MOSI, MISO, SCK, RadioResetpin
  cc1200.Configure(rxSniffSettings, rxSniffSettLen);  // 2sec internal delay
  cc1200.SetAddress(THIS_NODE);
  delay(10);
  cc1200.Strobe(CC120X_SCAL);
  delay(1000);
  byte readNode = cc1200.GetAddress(false);
  //Serial.print("\tNode: "); Serial.println(readNode);
  if (THIS_NODE == readNode) {
    //Serial.print("\tNode "); Serial.print(THIS_NODE); Serial.println(" Ok!");
  } else {
    //Serial.print("\tERROR \n");
    while (true)
      ;
  }
  cc1200.FlushRxFifo();
  delay(100);
  cc1200.FlushTxFifo();
  delay(100);
  //Serial.println("\tRadio Config");

  // RADIO INTERRUPT
  packetSemaphore = false;
  pinMode(RadioTXRXpin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(RadioTXRXpin), setSemaphore, FALLING);  // LOW, CHANGE, RISING, FALLING
  //Serial.println("\tPacket Interrupt Config");
  //Serial.flush();
}
void cc1200Transmit() {
  if (cc1200Check == 1) {
    writeBytes = FIFO_SIZE;  // idxFrameCount+1
    txBuffer[idxLength] = writeBytes;
    txBuffer[idxTargetNode] = TARG_NODE;
    txBuffer[idxFrameCount] = counterTx;
    for (int i = 0; i < FIFO_SIZE - 4; i++) {
      txBuffer[4 + i] = data.byteArray[i];
    }
    cc1200Check = 0;
    counterTx++;
    TryTransmit(TIMEOUT + millis());
  }
}

void setSemaphore() {
  packetSemaphore = true;
}

// Clear Packet Semaphore
void clearSemaphore() {
  packetSemaphore = false;
}

// Try transmitting outgoing, if any, within the TOUT duration.
bool TryTransmit(unsigned long qTimeout) {
  bool transmitStatus = false;
  byte marcstate = 0x00;
  //Serial.println("Wait Transmission...");

  do {
    marcstate = cc1200.GetStat(StatType::MARC_STATE, 0x1F);  // MARC_STATE[4:0]
    if (marcstate == MARC_STATE_IDLE) {
      cc1200.FlushTxFifo();
      delay(1);  // Flush ony in ERR or IDLE
      cc1200.WriteTxFifo(txBuffer, txBuffer[idxLength]);
      delay(1);
      cc1200.Transmit();
      delay(3);
      //Serial.println("\tTX");
    } else if (marcstate == MARC_STATE_TX_FIFO_ERR) {
      cc1200.FlushTxFifo();
      delay(1);
      //Serial.println("\tTXFIFO Flushed!");
    } else if (marcstate == MARC_STATE_RX_FIFO_ERR) {
      cc1200.FlushRxFifo();
      delay(1);
      //Serial.println("\tRXFIFO Flushed!");
    } else if (marcstate >= MARC_STATE_XOFF && marcstate <= MARC_STATE_ENDCAL) {
      //Serial.print("\tSettling: "); Serial.println(marcstate);
    }

    // Timeout Implementation
    if (millis() >= qTimeout)  //isTimeOut(qTimeout)
    {
      //Serial.println("\tTOUT!");
      cc1200.Idle();
      break;
    }

    delay(10);
  } while (!packetSemaphore);

  if (packetSemaphore) {
    cc1200.Idle();
    delay(1);  // Flush only in IDLE or FIFOERR
    cc1200.FlushTxFifo();
    transmitStatus = true;
    clearSemaphore();
  }

  return transmitStatus;
}

void floatAddByte(float data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}
void flashRead() {

  readFile(SPIFFS, "/Data.txt");
}

void flashClean() {

  deleteFile(SPIFFS, "/Data.txt");
}
void flashCreatPart() {
  writeFile(SPIFFS, "/Data.txt", All_data_flash1[0]);
}

void timerBegin() {
  ITimer1.attachInterruptInterval(20000, TimerHandler1);
}

void IRAM_ATTR TimerHandler1(void) {
  count = 1;
  countGps++;
  countCC1200++;
}

void listDir(fs::FS &fs, const char *dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.print(file.name());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.print(file.size());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
    }
    file = root.openNextFile();
  }
}

void createDir(fs::FS &fs, const char *path) {
  Serial.printf("Creating Dir: %s\n", path);
  if (fs.mkdir(path)) {
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

void removeDir(fs::FS &fs, const char *path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void readFile(fs::FS &fs, const char *path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return;
  }

  Serial.println("- read from file:");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_WRITE);
  file.print(message);
  file.close();
}

void appendFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_APPEND);
  file.print(message);
  file.close();
}

void deleteFile(fs::FS &fs, const char *path) {
  fs.remove(path);
}

void buzzer() {
  pinMode(15, OUTPUT);
  digitalWrite(15, 1);
  delay(400);
  digitalWrite(15, 0);
  delay(100);
  digitalWrite(15, 1);
  delay(400);
  digitalWrite(15, 0);
  delay(100);
  digitalWrite(15, 1);
  delay(400);
  digitalWrite(15, 0);
  delay(100);
}