#include <Wire.h>
#include <Adafruit_H3LIS331.h>
#include <Adafruit_Sensor.h>
#include "MS5611.h"
#include "SparkFun_BNO080_Arduino_Library.h"
#include "SparkFun_Ublox_Arduino_Library.h" //http://librarymanager/All#SparkFun_u-blox_GNSS
#include "FS.h"
#include "LITTLEFS.h"
#include <string>

#define SPIFFS LITTLEFS
#define FORMAT_LITTLEFS_IF_FAILED true
File file;
//sensor
Adafruit_H3LIS331 lis = Adafruit_H3LIS331();
SFE_UBLOX_GPS myGPS;
MS5611 MS5611(0x77);   // 0x76 = CSB to VCC; 0x77 = CSB to GND
sensors_event_t event;
BNO080 myIMU;

//sensor data 
float H3LIS331_data[3];
float Ms5611_data[2];
float x_Accel,y_Accel,z_Accel;
float x_Gyro,y_Gyro,z_Gyro;
float x_Mag,y_Mag,z_Mag;
float Roll,Pitch,Yaw;
long GPS_data[3];
int GPS_time[6];
byte SIV;
String All_data[5];
String header="400gAcceloX,400gAcceloY,400gAcceloZ,MS5611Temp,MS5611Pressure,BNOAccelX,BNOAccelY,BNOAccelZ,BNOGyroX,BNOGyroY,BNOGyroZ,BNOMagX,BNOMagY,BNOMagZ,Roll,Pitch,Yaw,GpsLatitude,GpsLongitude,GpsAltitude,GpsDate,GpsTime\n";
String dosya;
char Pdosya[200]="/Data1.txt";
int hafiza=0;
int i=0;
int count=0; 
int counter=2;
int countGps=0;
//hafıza
int Dosya_sayisi=200;
//timer
//hw_timer_t * timer = NULL;
//portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

//fonksiyon prototipleri
void sensorBegin();
void readSensor();
void flashRead();
void flashClean();
void flashCreatPart();
void flashWrite();
void sensorStart();
void readH3LIS331();
void readBno080();
void readMS5611();
void readGPS();
void writeData();
//void IRAM_ATTR onTime();
void listDir(fs::FS &fs, const char * dirname, uint8_t levels);
void createDir(fs::FS &fs, const char * path);
void removeDir(fs::FS &fs, const char * path);
void readFile(fs::FS &fs, const char * path);
void writeFile(fs::FS &fs, const char * path, String message);
void appendFile(fs::FS &fs, const char * path, String message);
void deleteFile(fs::FS &fs, const char * path);


void setup() {
  Serial.begin(115200);
  Wire.begin();
  Wire.setClock(400000);
  sensorBegin();

  //hafıza baslat
  SPIFFS.begin(1);
  flashRead();
  flashClean();
  //baslık dosyasını yaz
  writeFile(SPIFFS,"/Data1.txt", header);
  flashCreatPart();
  listDir(SPIFFS, "/", 0);
  dosya="/Data1.txt"; 
  //timer ayar
  //timerBegin();

}
unsigned long start=0;
unsigned long end=0;

void loop() {

  if(millis()-start>=20){
    start=millis();
    readSensor();
    end=millis();
    Serial.printf("okuma suresi: %d \n",end-start);
    //Serial.println(All_data);
  }
 /* 
  if (count > 0) {
    // Comment out enter / exit to deactivate the critical section 
    portENTER_CRITICAL(&timerMux);
    count=0;
    Serial.println("interrupt");
    portEXIT_CRITICAL(&timerMux);
    readSensor();
    
  }*/
}

void sensorBegin(){
  MS5611.begin();
  lis.begin_I2C();   // change this to 0x19 for alternative i2c address
  lis.setRange(H3LIS331_RANGE_100_G);   // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_50_HZ);

  myIMU.begin();
  myIMU.enableAccelerometer(50); //Send data update every 50ms
  myIMU.enableGyro(50); //Send data update every 50ms
  myIMU.enableMagnetometer(50); //Send data update every 50ms
  myIMU.enableRotationVector(50);
  
  //GPS ayar
  myGPS.begin();
  myGPS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)
  myGPS.setNavigationFrequency(1);
  myGPS.saveConfiguration();        //Save the current settings to flash and BBR
}

void flashRead(){
  for(int p=1;p<Dosya_sayisi;p++){
  dosya="/Data";
  dosya=dosya+String(p)+".txt";
  int str_len = dosya.length() + 1;
  char char_array[str_len]; 
  dosya.toCharArray(char_array, str_len);
  readFile(SPIFFS, char_array); 
  }
}

void flashClean(){
  for(int p=1;p<Dosya_sayisi+50;p++){
  dosya="/Data";
  dosya=dosya+String(p)+".txt";
  int str_len = dosya.length() + 1;
  char char_array[str_len]; 
  dosya.toCharArray(char_array, str_len);
  deleteFile(SPIFFS, char_array);
  
  }
}
void flashCreatPart(){
  for(int p=2;p<Dosya_sayisi;p++){
  dosya="/Data";
  dosya=dosya+String(p)+".txt";
  int str_len = dosya.length() + 1;
  char char_array[str_len]; 
  dosya.toCharArray(char_array, str_len);
  writeFile(SPIFFS, char_array, All_data[0]); 
  Serial.print("."); 
  } 
}
/*
void timerBegin(){
  timer = timerBegin(0, 80, true);                
  timerAttachInterrupt(timer, &onTime, true);    
  timerAlarmWrite(timer, 20000, true);           
  timerAlarmEnable(timer);
}
*/
//sensor okuma fonksiyonu
void readSensor(){
  sensorStart();
  readH3LIS331();
  readBno080();
  readMS5611();
  //readGPS();
  writeData();
  flashWrite();
}

/*
void IRAM_ATTR onTime() {

  portENTER_CRITICAL_ISR(&timerMux);
  count++;
  portEXIT_CRITICAL_ISR(&timerMux);
  
}
*/

void sensorStart(){
  MS5611.read();
  myIMU.dataAvailable();
  lis.getEvent(&event);  
}

void readH3LIS331(){
  H3LIS331_data[0]=event.acceleration.x/SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[1]=event.acceleration.y/SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[2]=event.acceleration.z/SENSORS_GRAVITY_STANDARD;
}

void readBno080(){
  x_Accel = myIMU.getAccelX();
  y_Accel = myIMU.getAccelY();
  z_Accel = myIMU.getAccelZ();
  x_Gyro = myIMU.getGyroX();
  y_Gyro = myIMU.getGyroY();
  z_Gyro = myIMU.getGyroZ();
  x_Mag = myIMU.getMagX();
  y_Mag = myIMU.getMagY();
  z_Mag = myIMU.getMagZ();
  Roll =(myIMU.getRoll()) * 180.0 / PI;
  Pitch = (myIMU.getPitch()) * 180.0 / PI;
  Yaw = (myIMU.getYaw()) * 180.0 / PI;
}

void readMS5611(){
  Ms5611_data[0]=MS5611.getTemperature();
  Ms5611_data[1]=MS5611.getPressure();
}

void readGPS(){

  if (countGps==10)
  {
    countGps=0;

    GPS_data[0] = myGPS.getLatitude();
    GPS_data[1] = myGPS.getLongitude();
    GPS_data[2] = myGPS.getAltitude();

    SIV = myGPS.getSIV();

    GPS_time[0]=myGPS.getYear();
    GPS_time[1]=myGPS.getMonth();
    GPS_time[2]=myGPS.getDay();

    GPS_time[3]=myGPS.getHour();
    GPS_time[4]=myGPS.getMinute();
    GPS_time[5]=myGPS.getSecond();
  }
  
  else
  {
    countGps++;
    GPS_data[0] = 0;
    GPS_data[1] = 0;
    GPS_data[2] = 0;

    SIV = 0;

    GPS_time[0] = 0;
    GPS_time[1] = 0;
    GPS_time[2] = 0;

    GPS_time[3] = 0;
    GPS_time[4] = 0;
    GPS_time[5] = 0;
  }
  
}

void writeData(){
  All_data[i]=String(H3LIS331_data[0])+","+String(H3LIS331_data[1])+","+String(H3LIS331_data[2])+",";

  All_data[i]+=String(Ms5611_data[0])+","+String(Ms5611_data[1])+",";

  All_data[i]+=String(x_Accel)+","+String(y_Accel)+","+String(z_Accel)+",";

  All_data[i]+=String(x_Gyro)+","+String(y_Gyro);All_data[i]+=","+String(z_Gyro)+",";

  All_data[i]+=String(x_Mag)+","+String(y_Mag)+","+String(z_Mag)+",";

  All_data[i]+=String(Roll)+","+String(Pitch)+","+String(Yaw)+",";
  
  All_data[i]+=String(GPS_data[0])+","+String(GPS_data[1])+","+String(GPS_data[2])+",";

  All_data[i]+=String(SIV)+",";

  All_data[i]+=String(GPS_time[0])+"-"+String(GPS_time[1])+"-"+String(GPS_time[2])+",";

  All_data[i]+=String(GPS_time[3])+"."+String(GPS_time[4])+"."+String(GPS_time[5])+"\n";

  if(i==4){
    i=0;
  }
  else{
    i++;
  }
}

void flashWrite(){
  if(i==4){
    if(hafiza==0){
      file = SPIFFS.open(Pdosya, FILE_APPEND);
      for(i=0;i<5;i++){
        file.print(All_data[i]);
      }
      Serial.println("acti");
      hafiza++;
    }
    else if(hafiza<200){
      for(i=0;i<5;i++){
        file.print(All_data[i]);
      }
      Serial.println("yazdi");
      hafiza++;
    }
    else{
      for(i=0;i<5;i++){
        file.print(All_data[i]);
      }
      file.close();
      hafiza=0;    
      Serial.println("kapadi");
      dosya="/Data";
      dosya=dosya+String(counter)+".txt";
      int str_len = dosya.length() + 1;
      dosya.toCharArray(Pdosya, str_len);  
      counter++;
    }
    i=0;
  }
  else{
    i++;
  }
}

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    Serial.printf("Listing directory: %s\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        Serial.println("Failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        Serial.println("Not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            Serial.print("  DIR : ");
            Serial.print (file.name());
            time_t t= file.getLastWrite();
            struct tm * tmstruct = localtime(&t);
            Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n",(tmstruct->tm_year)+1900,( tmstruct->tm_mon)+1, tmstruct->tm_mday,tmstruct->tm_hour , tmstruct->tm_min, tmstruct->tm_sec);
            if(levels){
                listDir(fs, file.name(), levels -1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("  SIZE: ");
            Serial.print(file.size());
            time_t t= file.getLastWrite();
            struct tm * tmstruct = localtime(&t);
            Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n",(tmstruct->tm_year)+1900,( tmstruct->tm_mon)+1, tmstruct->tm_mday,tmstruct->tm_hour , tmstruct->tm_min, tmstruct->tm_sec);
        }
        file = root.openNextFile();
    }
}

void createDir(fs::FS &fs, const char * path){
    Serial.printf("Creating Dir: %s\n", path);
    if(fs.mkdir(path)){
        Serial.println("Dir created");
    } else {
        Serial.println("mkdir failed");
    }
}

void removeDir(fs::FS &fs, const char * path){
    Serial.printf("Removing Dir: %s\n", path);
    if(fs.rmdir(path)){
        Serial.println("Dir removed");
    } else {
        Serial.println("rmdir failed");
    }
}

void readFile(fs::FS &fs, const char * path){
    Serial.printf("Reading file: %s\r\n", path);

    File file = fs.open(path);
    if(!file || file.isDirectory()){
        Serial.println("- failed to open file for reading");
        return;
    }

    Serial.println("- read from file:");
    while(file.available()){
        Serial.write(file.read());
    }
    file.close();
}

void writeFile(fs::FS &fs, const char * path, String message){
    File file = fs.open(path, FILE_WRITE);
    file.print(message);
    file.close();
}

void appendFile(fs::FS &fs, const char * path, String message){
    File file = fs.open(path, FILE_APPEND);
    file.print(message);
    file.close();
}

void deleteFile(fs::FS &fs, const char * path){
    fs.remove(path);
}