/*
   RadioLib SX126x Receive with Interrupts Example

   This example listens for LoRa transmissions and tries to
   receive them. Once a packet is received, an interrupt is
   triggered. To successfully receive data, the following
   settings have to be the same on both transmitter
   and receiver:
    - carrier frequency
    - bandwidth
    - spreading factor
    - coding rate
    - sync word

   Other modules from SX126x family can also be used.

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>
#include "SPI.h"
SPIClass SPI2(HSPI);

// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33);

// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;

//lora byte array
struct datas {
  byte byteArray[200];
  int counter = 0;
};
datas data;
 //byte to functions
uint8_t byteToUint8t(byte *byterray, int *count) {
  uint8_t f;
  ((uint8_t *)&f)[0] = byterray[*count];
  return f;
}

float byteToFloat(byte *byterray, int *count) {
  float f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
long byteToLong(byte *byterray, int *count) {
  long f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
unsigned long byteToULong(byte *byterray, int *count) {
  unsigned long f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt(byte *byterray, int *count) {
  int f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt16(byte *byterray, int *count) {
  int16_t f;
  for (int i = 0; i < 2; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToUint16(byte *byterray, int *count) {
  uint16_t f;
  for (int i = 0; i < 2; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
byte byteToByte(byte *byterray, int *count) {
  byte f;
  ((uint8_t *)&f)[0] = byterray[*count];
  (*count)++;
  return f;
}
 
void setup() {
  
  Serial.begin(115200);

  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin(868.0, 500.0, 7, 5, 0x34, 20);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }

  // set the function that will be called
  // when new packet is received
  radio.setDio1Action(setFlag);

  // start listening for LoRa packets
  Serial.print(F("[SX1262] Starting to listen ... "));
  state = radio.startReceive();
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }

  // if needed, 'listen' mode can be disabled by calling
  // any of the following methods:
  //
  // radio.standby()
  // radio.sleep()
  // radio.transmit();
  // radio.receive();
  // radio.readData();
  // radio.scanChannel();
}

// flag to indicate that a packet was received
volatile bool receivedFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// this function is called when a complete packet
// is received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if(!enableInterrupt) {
    return;
  }

  // we got a packet, set the flag
  receivedFlag = true;
}

void loop() {
  // check if the flag is set
  if(receivedFlag) {

    enableInterrupt = false;

    receivedFlag = false;

/*
byteToUint8t
byteToFloat
byteToLong
byteToULong
byteToLong
byteToInt
byteToInt16
byteToUint16
byteToByte
*/
    int state = radio.readData(data.byteArray,150);
    data.counter = 0;
    for(int a=0;a<70;a++){
      Serial.print(byteToInt16(data.byteArray, &data.counter));Serial.print(",");
    }
    Serial.println();
    
    if (state == ERR_NONE) {

    } else if (state == ERR_CRC_MISMATCH) {
      Serial.println(F("CRC error!"));

    } else {
      Serial.print(F("failed, code "));
      Serial.println(state);

    }

    radio.startReceive();
    enableInterrupt = true;
  }

}

