#include <SPI.h>
#include <Adafruit_H3LIS331.h>
#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>
#include "ms5611_driver.h"
#include "SparkFun_BNO080_Arduino_Library.h"
#include <Wire.h>
#include <RadioLib.h>
#include "SPI.h"
#include "SparkFun_Ublox_Arduino_Library.h"
#include <MicroNMEA.h>
char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));
SFE_UBLOX_GPS myGPS;
int RXD2 = 25;
int TXD2 = 26;
SPIClass SPI2(HSPI);
// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33, SPI2);

Adafruit_H3LIS331 lis = Adafruit_H3LIS331();
sensors_event_t event;
uint8_t accel200gCSPin = 27;
BNO080 myIMU;
uint8_t imuCSPin = 5;
uint8_t imuWAKPin = -1;
uint8_t imuINTPin = 16;
uint8_t imuRSTPin = 2;
uint8_t MS_CS = 17;
//#define SEALEVELPRESSURE_HPA (1013.25)
//Adafruit_BME280 bme(MS_CS);
baro_ms5611 MS5611(MS_CS);

float H3LIS331_data[3];
float Ms5611_data[2];
float bnoAccel[3];
float bnoGyro[3];
float bnoMag[3];
float bnoRaw[3];
long GPS_data[3];
int GPS_time[6];
uint8_t SIV;

void setup() {
  Serial.begin(115200);
  pinMode(imuCSPin, 1);
  pinMode(accel200gCSPin, 1);
  pinMode(MS_CS, 1);
  pinMode(imuRSTPin, 1);
  digitalWrite(imuCSPin, 1);
  digitalWrite(accel200gCSPin, 1);
  digitalWrite(MS_CS, 1);
  digitalWrite(imuRSTPin, 1);

  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;
  const int HSPI_CS = 15;
  SPI2.begin(HSPI_SCK, HSPI_MISO, HSPI_MOSI, HSPI_CS);
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin(868.0, 500.0, 7, 5, 0x34, 20);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }

  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  myGPS.begin(Serial2);
  myGPS.setSerialRate(38400);
  Serial.println("Trying 38400 baud");
  Serial2.begin(38400, SERIAL_8N1, RXD2, TXD2);
  myGPS.begin(Serial2);
  //This will pipe all NMEA sentences to the serial port so we can see them
  myGPS.setNMEAOutputPort(Serial);
  myGPS.setNavigationFrequency(10);

  myIMU.beginSPI(imuCSPin, imuWAKPin, imuINTPin, imuRSTPin);
  myIMU.enableAccelerometer(50);  //Send data update every 50ms
  myIMU.enableGyro(50);           //Send data update every 50ms
  myIMU.enableMagnetometer(50);   //Send data update every 50ms
  myIMU.enableRotationVector(100);

  if (!lis.begin_SPI(accel200gCSPin)) {
    Serial.println("Couldnt start");
    while (1) yield();
  }
  Serial.println("H3LIS331 found!");
  lis.setRange(H3LIS331_RANGE_100_G);  // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_50_HZ);

  MS5611.Initialize();
}
String All_data;
unsigned long start = 0;
unsigned long start2 = 0;
unsigned long end2 = 0;

void SFE_UBLOX_GPS::processNMEA(char incoming) {
  //Take the incoming char from the Ublox I2C port and pass it on to the MicroNMEA lib
  //for sentence cracking
  nmea.process(incoming);
}

void loop() {
  myIMU.dataAvailable();
  if (millis() - start > 100) {
    start = millis();
    bnoAccel[0] = myIMU.getAccelX() / SENSORS_GRAVITY_STANDARD;
    bnoAccel[1] = myIMU.getAccelY() / SENSORS_GRAVITY_STANDARD;
    bnoAccel[2] = myIMU.getAccelZ() / SENSORS_GRAVITY_STANDARD;
    bnoGyro[0] = myIMU.getGyroX();
    bnoGyro[1] = myIMU.getGyroY();
    bnoGyro[2] = myIMU.getGyroZ();
    bnoMag[0] = myIMU.getMagX();
    bnoMag[1] = myIMU.getMagY();
    bnoMag[2] = myIMU.getMagZ();
    bnoRaw[0] = (myIMU.getRoll()) * 180.0 / PI;
    bnoRaw[1] = (myIMU.getPitch()) * 180.0 / PI;
    bnoRaw[2] = (myIMU.getYaw()) * 180.0 / PI;


    sensors_event_t event;
    lis.getEvent(&event);
    H3LIS331_data[0] = event.acceleration.x / SENSORS_GRAVITY_STANDARD;
    H3LIS331_data[1] = event.acceleration.y / SENSORS_GRAVITY_STANDARD;
    H3LIS331_data[2] = event.acceleration.z / SENSORS_GRAVITY_STANDARD;


    MS5611.updateTemperature();
    MS5611.getPressure();
    Ms5611_data[0] = MS5611.getTemperature_degC();
    Ms5611_data[1] = MS5611.getPressure_mbar();

    int state = radio.transmit("Hello World!");

    // you can also transmit byte array up to 256 bytes long
    /*
    byte byteArr[] = {0x01, 0x23, 0x45, 0x56, 0x78, 0xAB, 0xCD, 0xEF};
    int state = radio.transmit(byteArr, 8);
       */

    if (state == ERR_NONE) {
      // the packet was successfully transmitted
      //Serial.println(F("success!"));

      // print measured data rate
      // Serial.print(F("[SX1262] Datarate:\t"));
      // Serial.print(radio.getDataRate());
      // Serial.println(F(" bps"));

    } else if (state == ERR_PACKET_TOO_LONG) {
      // the supplied packet was longer than 256 bytes
      Serial.println(F("too long!"));

    } else if (state == ERR_TX_TIMEOUT) {
      // timeout occured while transmitting packet
      Serial.println(F("timeout!"));

    } else {
      // some other error occurred
      Serial.print(F("failed, code "));
      Serial.println(state);
    }

    myGPS.checkUblox();  //See if new data is available. Process bytes as they come in.
    if (nmea.isValid() == true) {
      GPS_data[0] = nmea.getLatitude();
      GPS_data[1] = nmea.getLongitude();
      nmea.getAltitude(GPS_data[2]);
      SIV = nmea.getNumSatellites();
      GPS_time[0] = nmea.getYear();
      GPS_time[1] = nmea.getMonth();
      GPS_time[2] = nmea.getDay();
      GPS_time[3] = nmea.getHour();
      GPS_time[4] = nmea.getMinute();
      GPS_time[5] = nmea.getSecond();
    }

    end2 = millis();
    Serial.printf("okuma suresi: %d \n", end2 - start);

    All_data = String(H3LIS331_data[0]) + "," + String(H3LIS331_data[1]) + "," + String(H3LIS331_data[2]) + ",";
    All_data += String(Ms5611_data[0]) + "," + String(Ms5611_data[1]) + ",";
    All_data += String(bnoAccel[0]) + "," + String(bnoAccel[1]) + "," + String(bnoAccel[2]) + ",";
    All_data += String(bnoGyro[0]) + "," + String(bnoGyro[1]) + "," + String(bnoGyro[2]) + ",";
    All_data += String(bnoMag[0]) + "," + String(bnoMag[1]) + "," + String(bnoMag[2]) + ",";
    All_data += String(bnoRaw[0]) + "," + String(bnoRaw[1]) + "," + String(bnoRaw[2]) + ",";
    All_data += String(GPS_data[0]) + "," + String(GPS_data[1]) + "," + String(GPS_data[2]) + ",";
    All_data += String(SIV) + ",";
    All_data += String(GPS_time[0]) + "-" + String(GPS_time[1]) + "-" + String(GPS_time[2]) + ",";
    All_data += String(GPS_time[3]) + "." + String(GPS_time[4]) + "." + String(GPS_time[5]) + "\n";
    Serial.print(All_data);
  }
}
